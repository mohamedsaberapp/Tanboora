import { NgModule } from '@angular/core';
import { MycartComponent } from './mycart/mycart';
@NgModule({
	declarations: [MycartComponent],
	imports: [],
	exports: [MycartComponent]
})
export class ComponentsModule {}
