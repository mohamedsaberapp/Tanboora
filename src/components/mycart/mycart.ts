import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { CheckoutPage } from '../../pages/checkout/checkout';
/**
 * Generated class for the MycartComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mycart',
  templateUrl: 'mycart.html'
})
export class MycartComponent {

  myToken: any;
  subTotal: any;
  myCartInfo: any;
  shipping: number;
  myCart: any;
  myUser: any;
  cart: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public http: Http, private storage: Storage, private loadingCtrl:LoadingController) {
    this.myUser = {};
    this.myCartInfo={};
    this.myCart=[];
    this.shipping=100;
    //get user and cart
    this.storage.get('user_info').then((val) => {
      let loading = this.loadingCtrl.create({
        content:''
      })
      loading.present();
      console.log(val);
      this.myUser = val;
      this.myToken=val.id;
      //get cart
      this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
        console.log(data);
        this.myCartInfo=data;
        this.myCart = data.cart;
        this.subTotal=this.myCartInfo.cart_total+this.shipping;
        loading.dismiss();
      }, err => {
        console.log(err);
      });
    })

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  goCheckout() {
    this.navCtrl.push(CheckoutPage,{
      cart:this.myCart,
      total:this.subTotal
    });
  }
  removeItem(item){
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/remove_from_cart/?user_id=' + this.myToken + '&product_id=' + item.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.myCartInfo=data;
        this.myCart = data.cart;
        this.subTotal=this.myCartInfo.cart_total+this.shipping;
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }
}
