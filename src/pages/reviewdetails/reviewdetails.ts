import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ReviewPage } from '../review/review';


/**
 * Generated class for the ReviewdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reviewdetails',
  templateUrl: 'reviewdetails.html',
})
export class ReviewdetailsPage {

  myReviews: any[];
  ratee: number;
  part: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController) {
    this.myReviews=[];
    this.part=this.navParams.get('part');
    console.log(this.part);
    this.ratee=4;
    this.myReviews=this.part.reviews;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewdetailsPage');
  }
  openReview(){
    let modal=this.modalCtrl.create(ReviewPage,{
      shop_id:this.part.shop_id,
      product_id:this.part.id
    });
    modal.onDidDismiss(data => {
      console.log(data);
      this.myReviews=data;
    });
    modal.present();
  }

}
