import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Events } from 'ionic-angular';
import { ContactPage } from '../contact/contact';
import { SettingsPage } from '../settings/settings';
import { SupplierformPage } from '../supplierform/supplierform';
import { AboutPage } from '../about/about';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';
import { TermsPage } from '../terms/terms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { ReviewPage } from '../review/review';
/**
 * Generated class for the MorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {

  cartLength: any;
  myCart: any;
  myCartInfo: any;
  myUser: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController,private loadingCtrl:LoadingController,private http:Http,private storage:Storage,private toast:ToastController,public events:Events) {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          })
          loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
          let toast = this.toast.create({
            message: err,
            duration: 2000
          })
          toast.present();
        });
      }

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }
  goContact() {
    this.navCtrl.push(ContactPage);
  }
  goSettings() {
    this.navCtrl.push(SettingsPage);
  }
  goTerms(){
    this.navCtrl.push(TermsPage);
  }
  goSupplierForm() {
    this.navCtrl.push(SupplierformPage);
  }
  goAbout() {
    this.navCtrl.push(AboutPage);
  }
  openCart(){
    let modal = this.modalCtrl.create(CartPage);
    modal.onDidDismiss((data)=>{
      this.myCart=data;
      this.cartLength = this.myCart.length;
    });
    modal.present();
  }
  openContact(){
    this.navCtrl.push(ContactPage);
  }
  goReview(){
    this.navCtrl.push(ReviewPage);
  }
}
