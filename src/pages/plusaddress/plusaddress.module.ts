import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlusaddressPage } from './plusaddress';

@NgModule({
  declarations: [
    PlusaddressPage,
  ],
  imports: [
    IonicPageModule.forChild(PlusaddressPage),
  ],
})
export class PlusaddressPageModule {}
