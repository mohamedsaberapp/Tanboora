import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the PlusaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-plusaddress',
  templateUrl: 'plusaddress.html',
})
export class PlusaddressPage {

  myUser: any;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private storage: Storage, public http: Http, public events: Events) {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        loading.dismiss();
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlusaddressPage');
  }
  add(area,add) {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_address/?user_id=' + this.myUser.id + '&area=' + area + '&details=' + add).map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      loading.dismiss();
      this.navCtrl.pop();
    },err=>{
      console.log(err);
      loading.dismiss();
    })
  }
}
