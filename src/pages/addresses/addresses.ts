import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PlusaddressPage } from '../plusaddress/plusaddress';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the AddressesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addresses',
  templateUrl: 'addresses.html',
})
export class AddressesPage {

  myToken: any;
  myAddresses: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private http:Http,private loadingCtrl:LoadingController,public storage:Storage) {
    this.myAddresses=[];
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val.id){
        this.myToken=val.id;
        this.http.get('http://tanboora.bit68.com/user_info/?user_id='+this.myToken).map(res=>res.json()).subscribe(data=>{
          console.log(data);
          this.myAddresses=data.addresses;
          loading.dismiss();
        },err=>{
          console.log(err);
        })
      }
    })
  }

  ionViewDidEnter() {
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val.id){
        this.myToken=val.id;
        this.http.get('http://tanboora.bit68.com/user_info/?user_id='+this.myToken).map(res=>res.json()).subscribe(data=>{
          console.log(data);
          this.myAddresses=data.addresses;
          loading.dismiss();
        },err=>{
          console.log(err);
        })
      }
    })
  }
  address(){
    this.navCtrl.push(PlusaddressPage);
  }
  removeItem(e){
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.http.get('http://tanboora.bit68.com/remove_address/?user_id=' + this.myToken + '&address_id=' + e.id ).map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      loading.dismiss();
      this.myAddresses=data.addresses;
    },err=>{
      console.log(err);
      loading.dismiss();
    })
  }
}
