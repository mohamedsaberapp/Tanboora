import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { MygaragePage } from '../mygarage/mygarage';
import { ProfilePage } from '../profile/profile';
import { MorePage } from '../more/more';
import { SearchPage } from '../search/search';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = MygaragePage;
  tab3Root = SearchPage;
  tab4Root = ProfilePage;
  tab5Root = MorePage;

  constructor() {

  }
}
