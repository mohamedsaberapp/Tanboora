import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, LoadingController, Events } from 'ionic-angular';
import { SupplierPage } from '../supplier/supplier';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { ReviewdetailsPage } from '../reviewdetails/reviewdetails';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the PartdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-partdetails',
  templateUrl: 'partdetails.html',
})
export class PartdetailsPage {
  cartLength: any;
  myInfo: any;
  myCart: any;
  myToken: number;
  @ViewChild(Slides) slides: Slides;
  part: any;
  rate: number = 4;
  ratee: number = 4;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private http: Http, public storage: Storage,public events:Events) {
    this.myInfo = {};
    this.myCart = {};
    //get part details
    this.part = this.navParams.get('part');
    this.rate = 4;
    //get user_id
    this.storage.get('user_info').then((val) => {
      console.log(val);
      this.myInfo = val;
      if (val.id) {
        this.myToken = val.id;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myToken).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCart = data;
          this.cartLength=data.cart.length;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          })
        }, err => {
          console.log(err);
        });
      }
    })


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartdetailsPage');
  }
  viewSupplier(e) {
    this.navCtrl.push(SupplierPage,{shop_id:e});
  }
  openCart() {
    let modal = this.modalCtrl.create(CartPage);
    modal.present();
  }
  like(p) {
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_fav_product/?user_id=' + this.myToken + '&product_id=' + p.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      loading.dismiss();
      let added = this.toastCtrl.create({
        message: "تم الاضافة للمفضلة",
        duration: 2000
      })
      added.present()
    }, err => {
      console.log(err);
    });
  }
  goDetails(e) {
    this.navCtrl.push(ReviewdetailsPage, { part: e });
  }
  addToCart(p) {
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_to_cart/?user_id=' + this.myToken + '&product_id=' + p.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      loading.dismiss();
      this.events.publish('item:added', data);
      let added = this.toastCtrl.create({
        message: "تم الاضافة للسلة",
        duration: 2000
      })
      added.present()
    }, err => {
      console.log(err);
    });

  }
  onModelChange(e) {
    console.log(e);
  }
}
