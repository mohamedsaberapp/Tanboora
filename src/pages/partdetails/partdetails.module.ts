import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PartdetailsPage } from './partdetails';

@NgModule({
  declarations: [
    PartdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PartdetailsPage),
  ],
})
export class PartdetailsPageModule {}
