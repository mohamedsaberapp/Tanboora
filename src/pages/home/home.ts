import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, Events } from 'ionic-angular';
import { PartdetailsPage } from '../partdetails/partdetails';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';
import { ContactPage } from '../contact/contact';
import { SupplierPage } from '../supplier/supplier';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  ads: any;
  cartLength: any;
  myCart: any;
  myCartInfo: any;
  myUser: any;
  featuredProducts: any;
  featuredStores: any;
  products: any[];
  slides: any[];

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, public loadingCtrl: LoadingController, private storage: Storage, public http: Http,public events:Events) {
    
    this.slides = [
      { src: 'assets/imgs/slide2.jpg' },
      { src: 'assets/imgs/slide3.jpg' }
    ]
  }
  ionViewDidEnter(){
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          });
          this.events.subscribe('item:removed',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          });
          this.cartLength = data.cart.length; 
        }, err => {
          console.log(err);  
        });
      }
    })
    //featured_stores
    this.http.get('http://tanboora.bit68.com/featured_shops/').map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.featuredStores = data;
    }, err => {
      console.log(err);
    });

    //ads
    this.http.get('http://tanboora.bit68.com/ads_products/').map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.ads = data;
    }, err => {
      console.log(err);
    });

    //slider_images
    this.http.get('http://tanboora.bit68.com/home_slides/').map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.slides = data;
    }, err => {
      console.log(err);
    });

    //featured_products
    this.http.get('http://tanboora.bit68.com/featured_products/').map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.featuredProducts = data;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
  }
  openProd(p) {
    this.navCtrl.push(PartdetailsPage, { part: p });
  }
  openCart() {
    let modal = this.modalCtrl.create(CartPage);
    modal.onDidDismiss((data)=>{
      this.myCart=data;
      this.cartLength = this.myCart.length;
    });
    modal.present();
  }
  openContact() {
    this.navCtrl.push(ContactPage);
  }
  shopDetails(s) {
    this.navCtrl.push(SupplierPage, { shop: s });
  }

}
