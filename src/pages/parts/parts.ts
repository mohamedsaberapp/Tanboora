import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PartdetailsPage } from '../partdetails/partdetails';
import { FilterPage } from '../filter/filter';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the PartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parts',
  templateUrl: 'parts.html',
})
export class PartsPage {
  itemsTemp: any[];
  mods: any[];
  myParts: any;
  categories: any;
  brands: any;
  myCart: any;
  cartLength: any;
  myCartInfo: any;
  myCars: any;
  myToken: any;
  rate: number;
  filteredParts: { name: string; }[];
  items: any[];
  parts: any[];
  model: any;
  brand: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController,private storage:Storage,public http:Http,private loadingCtrl:LoadingController) {
    // this.items=[];
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    //categories
    this.http.get('http://tanboora.bit68.com/categories/').map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      this.categories=data;
    },err=>{
      console.log(err);
    });
    this.rate=4;
    this.parts=this.navParams.get('data');
    this.myParts=this.navParams.get('cat');
    this.initializeItems();
    console.log(this.parts,this.myParts);
    //get info and cars
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val){
        this.myToken=val.id;
        this.http.get('http://tanboora.bit68.com/user_info/?user_id='+this.myToken).map(res=>res.json()).subscribe(data=>{
          console.log(data);
          this.myCars=data.cars;
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          loading.dismiss();
        },err=>{
          console.log(err);
          loading.dismiss();
        })
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartsPage');
  }
  // getItems(e){
  //   console.log(e);
  // }

  initializeItems() {
    this.items = this.parts;
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  updateChoice(e){
    this.initializeItems();
    console.log(e);
    let carCategory = e;
  
    this.itemsTemp = this.items.filter(item => 
      { return item.category_id == carCategory}
    );
    this.items=this.itemsTemp;
    console.log(this.itemsTemp);  
  }
  goDetails(p){
    this.navCtrl.push(PartdetailsPage,{part:p});
  }
  goFilter(){
    let filter=this.modalCtrl.create(FilterPage);
    filter.present()
  }
  openCart(){
    let modal = this.modalCtrl.create(PartsPage);
    modal.present();
  }

}
