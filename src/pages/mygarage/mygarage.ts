import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, Events } from 'ionic-angular';
import { makeDecorator } from '@angular/core/src/util/decorators';
import { PartsPage } from '../parts/parts';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';
import { ContactPage } from '../contact/contact';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
/**
 * Generated class for the MygaragePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mygarage',
  templateUrl: 'mygarage.html',
})
export class MygaragePage {
  cartLength: any;
  myCart: any;
  myCartInfo: any;
  myYear: string;
  mods: any[];
  myModels: any[];
  myToken: any;
  myCars: any[];
  years: any[];
  models: any[];
  brands: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController,private loadingCtrl:LoadingController,private http:Http,private storage:Storage,private app:App,public events:Events) {
    this.myCars=[];
    this.myModels=[];
    this.mods=[];
    this.myYear="2018"; 
  }
  ionViewDidEnter(){
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    //get info and cars
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val){
        this.myToken=val.id;
        this.http.get('http://tanboora.bit68.com/user_info/?user_id='+this.myToken).map(res=>res.json()).subscribe(data=>{
          console.log(data);
          this.myCars=data.cars;
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          })
        },err=>{
          console.log(err);
        })
      }
    })
    //brands
    this.http.get('http://tanboora.bit68.com/car_makes/').map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      this.brands=data;
      loading.dismiss();
    },err=>{
      console.log(err);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MygaragePage');
  }
  proceed(b, m, y) {
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_user_car/?user_id=' + this.myToken + '&make_id=' + b + '&model_id=' + m + '&year=' + y).map(res=>res.json()).subscribe(data=>{
      console.log(data);
      this.myCars=data.cars;
      loading.dismiss();
    },err=>{
      console.log(err);
      loading.dismiss();
    })
  }
  openCart(){
    let modal = this.modalCtrl.create(CartPage);
    modal.onDidDismiss((data)=>{
      this.myCart=data;
      this.cartLength = this.myCart.length;
    });
    modal.present();
  }
  openContact(){
    this.navCtrl.push(ContactPage);
  }
  removeItem(item){
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/remove_user_car/?user_id=' + this.myToken + '&car_id=' + item.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.myCars = data.cars;
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }

  updateBrand(e){
    console.log(e);
    if(!isNaN(e)){
      let carModel = e;  
    this.mods = this.brands.filter(brand => 
      brand.id == carModel
    );
    console.log(this.mods);
    this.myModels = this.mods[0].models;
    console.log(this.myModels); 
    } 
  }
  goLog(){
    this.app.getRootNav().push(LoginPage);
  }
}
