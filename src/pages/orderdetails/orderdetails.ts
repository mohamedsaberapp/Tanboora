import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';

/**
 * Generated class for the OrderdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderdetails',
  templateUrl: 'orderdetails.html',
})
export class OrderdetailsPage {
  cancelled: string;
  deliv: string;
  Shipping: string;
  confirm: string;
  proc: string;
  subTotal: any;
  shipping: number;
  order: any;
  orderStatic: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController) {
    this.order=this.navParams.get('order');
    this.shipping=100;
    this.subTotal=this.order.total+this.shipping;
    this.proc='goldenrod';
    this.confirm='green';
    this.Shipping='#1970b9';
    this.deliv='#283044';
    this.cancelled='#db1e31'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderdetailsPage');
  }
  openCart(){
    let modal = this.modalCtrl.create(CartPage);
    modal.present();
  }
}
