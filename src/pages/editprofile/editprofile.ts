import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { CameraOptions, Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
  name:any;
  email:any;
  phone:any;
  myInfo: any;
  user_info: any;
  picId: string;
  myPic: any;
  u: any={};
  fileTransfer: FileTransferObject = this.transfer.create();
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,private storage:Storage,public http:Http,private loadingCtrl:LoadingController,public camera:Camera,public transfer:FileTransfer,private file:File,private toastCtrl:ToastController) {
    this.u=this.navParams.get('user');
    this.storage.get('user_info').then((val) => {
      this.user_info = val;
      console.log('user_info=',val);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }
  dismiss() {
    this.viewCtrl.dismiss(this.myInfo);
  }
  // edit() {
  //   this.viewCtrl.dismiss(this.myInfo);
  // }
  updateInfo(name,email,phone){
    console.log(name,email,phone);
    this.storage.get('user_info').then((val) => {
      this.user_info = val;
      console.log(this.user_info);
      this.http.get('http://tanboora.bit68.com/update_profile/?user_id=' + this.user_info.id + '&picture_id' + this.picId + '&name=' + name + '&email=' + email + '&phone=' + phone).map(res => res.json()).subscribe(data => {
        this.myInfo = data;
        this.u=data;
        this.storage.set('user_info',data);
        console.log('updated info', data);
        this.dismiss();
      }, err => {
        console.log(err);
        let toast=this.toastCtrl.create({
          message:err,
          duration:2000
        });
        toast.present();
        this.dismiss();
        
      })
    })
  }
  updatePic() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 300,
      targetHeight: 300,
    }

    this.camera.getPicture(options).then(file_uri => {
      console.log(file_uri);
      this.myPic = file_uri;
      let transfer_options: FileUploadOptions = {
        fileKey: 'file',
        fileName: 'name.jpeg',
        chunkedMode:false,
        mimeType:'image/jpeg'
     }
     let loading = this.loadingCtrl.create({
       content:''
     })
     loading.present();
     this.fileTransfer.upload(file_uri, 'http://tanboora.bit68.com/file_transfer/', transfer_options)
      .then((data:any) => { 
        console.log('transfer success', JSON.parse(data.response));
        this.picId=JSON.parse(data.response).id;
        this.http.get('http://tanboora.bit68.com/update_profile/?user_id=' + this.user_info.id + '&picture_id=' + this.picId).map(res => res.json()).subscribe(data => {
        loading.dismiss();
        console.log(data);
        console.log('updated info', data);
        this.u=data;
        // this.updateInfo(this.name,this.email,this.phone);
      }, err => {
        let toast=this.toastCtrl.create({
          message:err,
          duration:2000
        });
        toast.present();
        console.log(err);
      })
        // alert('transfer success' + data)
      }, (err) => {
        let toastTransfer=this.toastCtrl.create({
          message:err,
          duration:2000
        });
        toastTransfer.present();
        console.log('transfer err',err)
        // alert('transfer err' + err);
      })

    }, err => {
      let toastGetPic=this.toastCtrl.create({
        message:err,
        duration:2000
      });
      toastGetPic.present();
      console.log('get picture', err);
      // alert('get Picture err'+ err);
      // Handle error
    });
  }
}
