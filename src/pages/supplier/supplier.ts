import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ReviewPage } from '../review/review';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { PartdetailsPage } from '../partdetails/partdetails';
/**
 * Generated class for the SupplierPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-supplier',
  templateUrl: 'supplier.html',
})
export class SupplierPage {
  temp: any;
  shop_id: any;
  reviews: any;
  cartLength: any;
  myCart: any;
  myCartInfo: any;
  myUser: any;
  shop: any;
  suppItems: any[];


  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private loadingCtrl: LoadingController, private http: Http, private storage: Storage) {
    this.reviews=[];
    this.shop={};
    this.suppItems=[];
    this.temp={};
    this.temp=this.navParams.get('shop');
    if(this.temp!=undefined){
      console.log(this.temp);
      this.shop=this.temp;
      this.suppItems = this.shop.products;
      this.reviews = this.shop.reviews;
    }else{
      this.shop_id=this.navParams.get('shop_id');
      this.http.get('http://tanboora.bit68.com/shop?shop_id=' + this.shop_id).map(res=>res.json()).subscribe((data:any)=>{
        console.log(data);
        this.shop=data;
        this.suppItems=this.shop.products;
        this.reviews = this.shop.reviews;  
      },err=>{
        console.log(err);
      })
    }
    // console.log(this.navParams.get('shop'));
    // console.log(this.navParams.get('shop_id'));
    // if (this.navParams.get('shop')!=='undefined') {
    //   this.shop = this.navParams.get('shop');
    //   this.suppItems = this.shop.products;
    //   this.reviews = this.shop.reviews;
    // } else if(this.navParams.get('shop_id')!=='undefined') {
    //   this.shop_id=this.navParams.get('shop_id');
    //   this.http.get('http://tanboora.bit68.com/shop?shop_id=' + this.shop_id).map(res=>res.json()).subscribe((data:any)=>{
    //     this.shop=data;
    //     this.suppItems=this.shop.products;
    //     this.reviews = this.shop.reviews;
    //     console.log(data);
    //   },err=>{
    //     console.log(err);
    //   })
    // }else{
    //   console.log('err');
    // }
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
      }

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupplierPage');
  }
  addReview() {
    let review = this.modalCtrl.create(ReviewPage, { shop: this.shop });
    review.present()
  }
  openCart() {
    let modal = this.modalCtrl.create(CartPage);
    modal.present();
  }
  openProd(p) {
    this.navCtrl.push(PartdetailsPage, { part: p });
  }
  react(r) {
    console.log(r);
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_review_like/?user_id=' + this.myUser.id + '&review_id=' + r.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.reviews = data;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
  }
}
