import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OrderdetailsPage } from '../orderdetails/orderdetails';

/**
 * Generated class for the MyordersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myorders',
  templateUrl: 'myorders.html',
})
export class MyordersPage {

  orders: { num: string; stat: string; color: string; }[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.orders=this.navParams.get('orders');
  }
  goDetails(order){
    this.navCtrl.push(OrderdetailsPage,{order:order});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyordersPage');
  }

}
