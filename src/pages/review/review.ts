import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review',
  templateUrl: 'review.html',
})
export class ReviewPage {
  myData: any;
  myPartId: any;
  myShopId: any;
  myShop: any;
  myUser: any;
  rate: number = 1;
  review: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,public storage:Storage,private http:Http,private loadingCtrl:LoadingController) {
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if(val){
        this.myUser = val;
      }
    })
    this.myShop=this.navParams.get('shop');
    this.myShopId=this.navParams.get('shop_id');
    this.myPartId=this.navParams.get('product_id');
    console.log(this.myShopId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPage');
  }
  onModelChange(e) {
    console.log(e);
  }
  submit(rate,review) {
    console.log(rate,review);
    let loading = this.loadingCtrl.create({
      content:''
    });
    loading.present();
    this.http.get('http://tanboora.bit68.com/add_review/?user_id=' + this.myUser.id + '&shop_id=' + this.myShopId + '&review_text=' + review + '&review_rate=' + rate + '&product_id=' + this.myPartId).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myData=data;
          loading.dismiss();
          this.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
    
  }
  dismiss() {
    this.viewCtrl.dismiss(this.myData);
  }
}
