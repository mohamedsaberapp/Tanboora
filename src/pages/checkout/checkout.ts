import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { PlusaddressPage } from '../plusaddress/plusaddress';
import { HomePage } from '../home/home';
/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  addresses: any;
  name: any;
  address: any;
  phone: any;
  myUser: any;
  subTotal: any;
  shipping: number;
  total: any;
  showVisa: boolean = false;
  order: any=[];
  payment: string = "cash";
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage:Storage,private http:Http,private loadingCtrl:LoadingController,private toastCtrl:ToastController) {
    this.shipping=100;
    this.addresses=[];
    this.order=this.navParams.get('cart');
    this.total=this.navParams.get('total');
    this.subTotal=this.total+this.shipping;
    

    
  }

  ionViewDidEnter() {
    let loading = this.loadingCtrl.create({
      content:''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        //get cart
        this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myUser = data;
          this.phone=data.phone;
          this.addresses=data.addresses;
          this.address=data.addresses[0].id;
          this.name=data.name;
          loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
      }

    })
  }
  goPayment(tot,name,add,phone) {
    let loading = this.loadingCtrl.create({
      content:''
    });
    loading.present();
    console.log(tot,name,add,phone);
    this.http.get('http://tanboora.bit68.com/place_order/?user_id=' + this.myUser.id + '&name=' + name + '&address_id=' + add + '&phone=' + phone + '&total=' + tot).map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      loading.dismiss();
      let order = this.toastCtrl.create({
        message: "تم ارسال طلبك في انتظار التأكيد",
        duration: 2000
      })
      order.present();
      this.navCtrl.setRoot(TabsPage);
    },err=>{
      console.log(err);
      loading.dismiss();
    })
  }
  goAddress(){
    this.navCtrl.push(PlusaddressPage);
  }

}
