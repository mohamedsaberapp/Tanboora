import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  filteredParts: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    this.filteredParts=[
      {name:'Airbags'},
      {name:'Brakes'},
      {name:'Accessories'},
      {name:'Engines'},
      {name:'Cooling'},
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }
  submit(opt) {
    console.log(opt);
    this.viewCtrl.dismiss();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
