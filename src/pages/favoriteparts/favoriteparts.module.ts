import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritepartsPage } from './favoriteparts';

@NgModule({
  declarations: [
    FavoritepartsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritepartsPage),
  ],
})
export class FavoritepartsPageModule {}
