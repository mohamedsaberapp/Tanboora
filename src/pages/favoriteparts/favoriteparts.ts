import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PartdetailsPage } from '../partdetails/partdetails';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the FavoritepartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoriteparts',
  templateUrl: 'favoriteparts.html',
})
export class FavoritepartsPage {

  myFavParts: any=[];
  myToken: any;
  parts: { src: string; name: string; }[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public http:Http,private loadingCtrl:LoadingController) {
    this.myFavParts=[];
    let loading = this.loadingCtrl.create({
      content:''
    })
    loading.present();
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val.id){
        this.myToken=val.id;
        this.http.get('http://tanboora.bit68.com/user_info/?user_id='+this.myToken).map(res=>res.json()).subscribe(data=>{
          console.log(data);
          this.myFavParts=data.fav_products;
          loading.dismiss();
        },err=>{
          console.log(err);
        })
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritepartsPage');
  }
  partDetails(p){
    this.navCtrl.push(PartdetailsPage,{part:p})
  }
  removeItem(item){
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/remove_fav_product/?user_id=' + this.myToken + '&product_id=' + item.id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.myFavParts = data.fav_products;
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }

}
