import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myUser: any;
  isLoggedIn: boolean;
  email: any;
  password: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage: Storage, private loadingCtrl: LoadingController, private toast: ToastController, public fb: Facebook) {
    fb.getLoginStatus()
      .then(res => {
        console.log(res.status);
        if (res.status === "connect") {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(e, p) {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.http.get('http://tanboora.bit68.com/login/?email=' + e + '&password=' + p).map(res => res.json()).subscribe((data: any) => {
      loading.dismiss();
      console.log(data);
      if (data.name) {
        this.myUser = data;
        this.storage.set('user_info', data);
        this.navCtrl.setRoot(TabsPage);
      } else {
        let toast = this.toast.create({
          message: data._body,
          duration: 2000
        })
        toast.present();
      }
    }, err => {
      loading.dismiss();
      let toast = this.toast.create({
        message: err.statusText,
        duration: 2000
      })
      toast.present();
      console.log(err);
    })
  }
  goReg() {
    this.navCtrl.push(RegisterPage);
  }
  loginFB() {
    let loading = this.loadingCtrl.create({
      content: ''
    });
    loading.present();
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        loading.dismiss();
        if (res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
        console.log('Logged into Facebook!', res);
      })
      .catch(e => {
        loading.dismiss();
        console.log('Error logging into Facebook', e);
        let toast = this.toast.create({
          message: e,
          duration: 2000
        })
        toast.present();
      });
  }
  getUserDetail(userid) {
    this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
      .then(res => {
        console.log(res);
        this.http.get('http://tanboora.bit68.com/login/?fb_token=' + userid + '&email=' + res.email + '&name=' + res.name + '&image=' + res.picture.data.url).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          if(data.id){
            this.storage.set('user_info',data).then((val)=>{
              console.log('storage value',val);
              this.navCtrl.setRoot(TabsPage);
            })
          }else{
            let toast = this.toast.create({
              message: data._body,
              duration: 2000
            })
            toast.present();
          }
        }, err => {
          console.log(err);
          let toast = this.toast.create({
            message: err,
            duration: 2000
          })
          toast.present();
        })
      })
      .catch(e => {
        console.log(e);
      });
  }
}
