import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, Events } from 'ionic-angular';
import { CheckoutPage } from '../checkout/checkout';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  myToken: any;
  subTotal: any;
  myCartInfo: any;
  shipping: number;
  myCart: any;
  myUser: any;
  cart: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public http: Http, private storage: Storage, private loadingCtrl: LoadingController,private events:Events) {
    this.myUser = {};
    this.myCartInfo = {};
    this.myCart = [];
    this.shipping = 100;
    //get user and cart
    this.storage.get('user_info').then((val) => {
      let loading = this.loadingCtrl.create({
        content: ''
      })
      loading.present();
      console.log(val);
      this.myUser = val;
      this.myToken = val.id;
      //get cart
      this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
        console.log(data);
        this.myCartInfo = data;
        this.myCart = data.cart;
        this.events.subscribe('item:added',(data)=>{
          console.log(data);
          this.myCart=data.cart;
        })
        this.events.subscribe('item:removed',(data)=>{
          console.log(data);
          this.myCart=data.cart;
        })
        this.subTotal = this.myCartInfo.cart_total + this.shipping;
        
        loading.dismiss();
      }, err => {
        console.log(err);
      });
    })

  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }
  dismiss() {
    this.viewCtrl.dismiss(this.myCart);
  }
  goCheckout() {
    this.navCtrl.push(CheckoutPage, {
      cart: this.myCart,
      total: this.myCartInfo.cart_total
    });
  }
  removeItem(item) {
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/remove_from_cart/?user_id=' + this.myToken + '&cart_item_id=' + item.cart_item_id).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.events.publish('item:removed', data);
      this.myCartInfo = data;
      this.myCart = data.cart;
      this.subTotal = this.myCartInfo.cart_total + this.shipping;
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }
  goHome(){
    this.dismiss();
  }
}
