import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, App, LoadingController, Events } from 'ionic-angular';
import { PartsPage } from '../parts/parts';
import { CartPage } from '../cart/cart';
import { ContactPage } from '../contact/contact';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  myUser: any;
  cartLength: any;
  myCart: any;
  myCartInfo: any;
  categories: any;
  myYear: string;
  myModels: any;
  mods: any[];
  myToken: string;
  parts: any[];
  brands: any[];
  models: any[];
  years: any[];
  myCars: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private loadingCtrl: LoadingController, private http: Http, private storage: Storage, private app: App,public events:Events) {
    this.myModels=[];
    this.mods=[];
    this.myYear="2018";
    let loading = this.loadingCtrl.create({
      content:''
    });
    loading.present();
    this.storage.get('user_info').then((val)=>{
      console.log(val);
      if(val){
        this.myToken=val.id;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myToken).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          })
        }, err => {
          console.log(err);
        });
      }
    })
    //brands
    this.http.get('http://tanboora.bit68.com/car_makes/').map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      this.brands=data;
    },err=>{
      console.log(err);
    });

    //categories
    this.http.get('http://tanboora.bit68.com/categories/').map(res=>res.json()).subscribe((data:any)=>{
      console.log(data);
      this.categories=data;
      loading.dismiss();
    },err=>{
      console.log(err);
    });
    
    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  proceed(b, m, y,cat) {
    let loading = this.loadingCtrl.create({
      content: ''
    })
    loading.present();
    this.http.get('http://tanboora.bit68.com/search/?user_id=' + this.myToken + '&make_id=' + b + '&model_id=' + m ).map(res => res.json()).subscribe(data => {
      console.log(data);
      loading.dismiss();
      this.navCtrl.push(PartsPage,{
        data:data,
        cat:cat
      })
    }, err => {
      console.log(err);
      loading.dismiss();
    })

  }
  openCart() {
    let modal = this.modalCtrl.create(CartPage);
    modal.onDidDismiss((data)=>{
      this.myCart=data;
      this.cartLength = this.myCart.length;
    });
    modal.present();
  }
  openContact() {
    this.navCtrl.push(ContactPage);
  }
  updateBrand(e){
    console.log(e);
    let carModel = e;
  
    this.mods = this.brands.filter(brand => 
      brand.id == carModel
    );
    console.log(this.mods);
    this.myModels = this.mods[0].models;
    console.log(this.myModels);  
    
  }

}
