import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Events, LoadingController } from 'ionic-angular';
import { OrderdetailsPage } from '../orderdetails/orderdetails';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CartPage } from '../cart/cart';
import { MyordersPage } from '../myorders/myorders';
import { AuthPage } from '../auth/auth';
import { FavoritepartsPage } from '../favoriteparts/favoriteparts';
import { ContactPage } from '../contact/contact';
import { AddressesPage } from '../addresses/addresses';
import { EditprofilePage } from '../editprofile/editprofile';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  u: any;
  cartLength: any;
  myCart: any;
  myCartInfo: any;
  myUser: any;
  orders: any[];
  favorites: any[];
choice:string="fav";
  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController,private app:App,private storage:Storage,private http:Http,public events:Events,private loadingCtrl:LoadingController) {
    this.myUser={};
    this.choice="fav";  
  }
  ionViewDidEnter(){
    console.log('entered')
    let loading=this.loadingCtrl.create({
      content:''
    });
    loading.present();
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myUser = val;
        this.u=val;
        //get cart
        this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + this.myUser.id).map(res => res.json()).subscribe((data: any) => {
          console.log(data);
          this.myCartInfo = data;
          this.myCart = data.cart;
          this.cartLength = data.cart.length;
          this.orders=data.orders;
          this.events.subscribe('item:added',(data)=>{
            this.myCart=data.cart;
            this.cartLength=data.cart.length;
          })
          loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
      }else{
        loading.dismiss();
      }

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  goDetails(order){
    this.navCtrl.push(OrderdetailsPage,{order:order});
  }
  openCart(){
    let modal = this.modalCtrl.create(CartPage);
    modal.onDidDismiss((data)=>{
      this.myCart=data;
      this.cartLength = this.myCart.length;
    });
    modal.present();
  }
  goOrders(){
    this.navCtrl.push(MyordersPage,{orders:this.orders});
  }
  goFav(f){
    this.navCtrl.push(FavoritepartsPage,{favs:f});
  }
  logOut(){
    this.storage.clear().then((val:void)=>{
      this.app.getRootNav().setRoot(AuthPage);
    })
    
  }
  openContact(){
    this.navCtrl.push(ContactPage);
  }
  goAddress(ad){
    this.navCtrl.push(AddressesPage,{adds:ad});
  }
  editProfile(){
    let modalProf = this.modalCtrl.create(EditprofilePage,{user:this.u});
    modalProf.onDidDismiss(data=>{
      console.log(data);
    })
    modalProf.present();
  }
  goLog(){
    this.app.getRootNav().setRoot(LoginPage);
  }
}
