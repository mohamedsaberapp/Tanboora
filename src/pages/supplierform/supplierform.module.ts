import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupplierformPage } from './supplierform';

@NgModule({
  declarations: [
    SupplierformPage,
  ],
  imports: [
    IonicPageModule.forChild(SupplierformPage),
  ],
})
export class SupplierformPageModule {}
