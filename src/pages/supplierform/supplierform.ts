import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

/**
 * Generated class for the SupplierformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-supplierform',
  templateUrl: 'supplierform.html',
})
export class SupplierformPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupplierformPage');
  }
  submit(){
    let toast=this.toastCtrl.create({
      message:'Your informations have been sent ! waiting for review',
      duration:3000
    });
    toast.present()
  }

}
