import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AuthPage } from '../pages/auth/auth';
import { CartPage } from '../pages/cart/cart';
import { CheckoutPage } from '../pages/checkout/checkout';
import { FilterPage } from '../pages/filter/filter';
import { MygaragePage } from '../pages/mygarage/mygarage';
import { OrderdetailsPage } from '../pages/orderdetails/orderdetails';
import { PartdetailsPage } from '../pages/partdetails/partdetails';
import { PartsPage } from '../pages/parts/parts';
import { ReviewPage } from '../pages/review/review';
import { SupplierPage } from '../pages/supplier/supplier';
import { ProfilePage } from '../pages/profile/profile';
import { Ionic2RatingModule } from 'ionic2-rating';
import { PaymentPage } from '../pages/payment/payment';
import { MorePage } from '../pages/more/more';
import { SettingsPage } from '../pages/settings/settings';
import { SupplierformPage } from '../pages/supplierform/supplierform';
import { SearchPage } from '../pages/search/search';
import { TermsPage } from '../pages/terms/terms';
import { MyordersPage } from '../pages/myorders/myorders';
import { FavoritepartsPage } from '../pages/favoriteparts/favoriteparts';
import { SplashPage } from '../pages/splash/splash';
import { AddressesPage } from '../pages/addresses/addresses';
import { ReviewdetailsPage } from '../pages/reviewdetails/reviewdetails';
import { PlusaddressPage } from '../pages/plusaddress/plusaddress';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';
import { MycartComponent } from '../components/mycart/mycart';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    AuthPage,
    CartPage,
    CheckoutPage,
    FilterPage,
    MygaragePage,
    OrderdetailsPage,
    PartdetailsPage,
    PartsPage,
    ReviewPage,
    SupplierPage,
    ProfilePage,
    PaymentPage,
    MorePage,
    SettingsPage,
    SupplierformPage,
    SearchPage,
    TermsPage,
    MyordersPage,
    FavoritepartsPage,
    SplashPage,
    AddressesPage,
    ReviewdetailsPage,
    PlusaddressPage,
    EditprofilePage,
    MycartComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    AuthPage,
    CartPage,
    CheckoutPage,
    FilterPage,
    MygaragePage,
    OrderdetailsPage,
    PartdetailsPage,
    PartsPage,
    ReviewPage,
    SupplierPage,
    ProfilePage,
    PaymentPage,
    MorePage,
    SettingsPage,
    SupplierformPage,
    SearchPage,
    TermsPage,
    MyordersPage,
    FavoritepartsPage,
    SplashPage,
    AddressesPage,
    ReviewdetailsPage,
    PlusaddressPage,
    EditprofilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    Camera,
    FileTransfer,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
