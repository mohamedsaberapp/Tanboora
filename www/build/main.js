webpackJsonp([27],{

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
        modal.present();
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/about/about.html"*/'<!--\n  Generated template for the AboutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="caramel">\n    <ion-title>عن طنبورة</ion-title>\n    <ion-buttons dir="ltr" end>\n        <button (click)="openCart()" ion-button icon-only color="royal">\n          <ion-icon name="cart"><ion-badge color="secondary">3</ion-badge></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div text-center>\n    <img small src="assets/imgs/tanboora.png">\n  </div>\n  <div padding>\n    <p> طنبورة هو متجر إلكتروني لقطع غيار السيارات. نحن نوصلك لموردين قطاع الغيار المحترفين في مصر . لضمان أحسن الأسعار وأفضل جودة</p>\n  </div>\n\n\n</ion-content>\n<ion-footer>\n    <p ion-text color="gray" text-center>Developed & maintained by <a href="https://bit68.com">Bit68</a></p>\n  </ion-footer>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/about/about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewdetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__review_review__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ReviewdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReviewdetailsPage = (function () {
    function ReviewdetailsPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.myReviews = [];
        this.part = this.navParams.get('part');
        console.log(this.part);
        this.ratee = 4;
        this.myReviews = this.part.reviews;
    }
    ReviewdetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReviewdetailsPage');
    };
    ReviewdetailsPage.prototype.openReview = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__review_review__["a" /* ReviewPage */], {
            shop_id: this.part.shop_id,
            product_id: this.part.id
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            _this.myReviews = data;
        });
        modal.present();
    };
    ReviewdetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-reviewdetails',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/reviewdetails/reviewdetails.html"*/'<ion-header>\n  <ion-navbar color="caramel">\n    <ion-title>{{part.name}}</ion-title>\n    <!-- <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">٣</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n  <ion-row padding>\n    <h5 ion-text color="primary">تعليقات / التقييمات</h5>\n  </ion-row>\n  <ion-row class="body whiteBg">\n    <ion-col text-center col-5>\n      <span class="smText" ion-text color="gray">معدل التقدير</span>\n      <h4>3.8</h4>\n      <span class="smText" ion-text color="gray">من ١١ ذبون</span>\n    </ion-col>\n    <ion-col col-7 text-center>\n      <ion-row>\n        <ion-col col-2>(11)</ion-col>\n        <ion-col class="container noPadding" col-8>\n          <div class="skills css"></div>\n        </ion-col>\n        <ion-col col-2>5</ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-2>(14)</ion-col>\n        <ion-col class="container noPadding" col-8>\n          <div class="skills css"></div>\n        </ion-col>\n        <ion-col col-2>4</ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-2>(31)</ion-col>\n        <ion-col class="container noPadding" col-8>\n          <div class="skills css"></div>\n        </ion-col>\n        <ion-col col-2>3</ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-2>(21)</ion-col>\n        <ion-col class="container noPadding" col-8>\n          <div class="skills css"></div>\n        </ion-col>\n        <ion-col col-2>2</ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-2>(10)</ion-col>\n        <ion-col class="container noPadding" col-8>\n          <div class="skills css"></div>\n        </ion-col>\n        <ion-col col-2>1</ion-col>\n      </ion-row>\n      <br>\n      <ion-row >\n        <ion-col text-center>\n            <strong  ion-text color="primary" (click)="openReview()">كتابة تعليق</strong>\n        </ion-col>\n        \n      </ion-row>\n    </ion-col>\n  </ion-row>\n  <ion-row padding>\n    <h5 ion-text color="primary">تقييمات المستخدم</h5>\n  </ion-row>\n  <ion-list>\n    <ion-item *ngIf="!myReviews[0]">\n      <p text-center>لا توجد تقييمات</p>\n    </ion-item>\n    <div  *ngIf="myReviews[0]">\n        <ion-item *ngFor="let rev of myReviews">\n            <div>\n              <rating class="rating" [(ngModel)]="rev.rate" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half"\n                starIconName="star" nullable="false"></rating>\n              <span class="smText fl_left">{{rev.date}}<br>{{rev.time}}</span>\n            </div>\n            <h4>{{rev.user_name}}</h4>\n            <p class="full">{{rev.text}}</p>\n          </ion-item>\n    </div>  \n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/reviewdetails/reviewdetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], ReviewdetailsPage);
    return ReviewdetailsPage;
}());

//# sourceMappingURL=reviewdetails.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MygaragePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the MygaragePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MygaragePage = (function () {
    function MygaragePage(navCtrl, navParams, modalCtrl, loadingCtrl, http, storage, app, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.app = app;
        this.events = events;
        this.myCars = [];
        this.myModels = [];
        this.mods = [];
        this.myYear = "2018";
    }
    MygaragePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        //get info and cars
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myToken = val.id;
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCars = data.cars;
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                }, function (err) {
                    console.log(err);
                });
            }
        });
        //brands
        this.http.get('http://tanboora.bit68.com/car_makes/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.brands = data;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    MygaragePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MygaragePage');
    };
    MygaragePage.prototype.proceed = function (b, m, y) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_user_car/?user_id=' + this.myToken + '&make_id=' + b + '&model_id=' + m + '&year=' + y).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myCars = data.cars;
            loading.dismiss();
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    MygaragePage.prototype.openCart = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
        modal.onDidDismiss(function (data) {
            _this.myCart = data;
            _this.cartLength = _this.myCart.length;
        });
        modal.present();
    };
    MygaragePage.prototype.openContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_contact__["a" /* ContactPage */]);
    };
    MygaragePage.prototype.removeItem = function (item) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/remove_user_car/?user_id=' + this.myToken + '&car_id=' + item.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myCars = data.cars;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    MygaragePage.prototype.updateBrand = function (e) {
        console.log(e);
        if (!isNaN(e)) {
            var carModel_1 = e;
            this.mods = this.brands.filter(function (brand) {
                return brand.id == carModel_1;
            });
            console.log(this.mods);
            this.myModels = this.mods[0].models;
            console.log(this.myModels);
        }
    };
    MygaragePage.prototype.goLog = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]);
    };
    MygaragePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-mygarage',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/mygarage/mygarage.html"*/'<!--\n  Generated template for the MygaragePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>\n      <img class="logoBar" src="assets/imgs/tanboora.png">\n    </ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons start>\n      <button (click)="openContact()" ion-button icon-only color="royal">\n        <ion-icon name="call">          \n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list no-lines class="form">\n    <ion-item class="transparent">\n      <!-- <ion-label>Brand</ion-label> -->\n      <ion-select class="whiteBg" block interface="action-sheet" [(ngModel)]="myBrand" (ngModelChange)="updateBrand(myBrand)">\n        <ion-option ion-text color="light" selected>ماركة </ion-option>\n        <ion-option ion-text color="light" *ngFor="let brand of brands" value="{{brand.id}}">{{brand.title}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item class="transparent">\n      <!-- <ion-label>Model</ion-label> -->\n      <ion-select class="whiteBg" interface="action-sheet" block [(ngModel)]="myModel">\n          <ion-option ion-text color="light" selected>موديل</ion-option>\n        <ion-option ion-text color="light" *ngFor="let model of myModels" value="{{model.id}}">{{model.title}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item class="transparent">\n      <!-- <ion-label>Year</ion-label> -->\n      <!-- <ion-select class="whiteBg" interface="action-sheet" [(ngModel)]="myYear">\n          <ion-option ion-text color="light" selected>سنة</ion-option>\n        <ion-option ion-text color="light" *ngFor="let year of years">{{year.title}}</ion-option>\n        \n      </ion-select> -->\n      <ion-datetime class="whiteBg" displayFormat="YYYY" pickerFormat="YYYY" [(ngModel)]="myYear" min="1981"></ion-datetime>\n    </ion-item>\n    <br>\n    <div padding>\n      <button [disabled]="!myToken || !myBrand || !myModel || !myYear" ion-button block color="primary" (click)="proceed(myBrand,myModel,myYear)">إضافة</button>\n    </div>\n  </ion-list>\n  <div padding text-center *ngIf="!myCars[0]">\n      <ion-icon class="bigIcon" name="car"></ion-icon>\n      <br>\n      <p *ngIf="myToken" ion-text color="gray" text-center>لا توجد اي سيارات خاصة بك </p>\n      <p *ngIf="!myToken">لإضافة سيارة تحتاج للتسجيل</p>\n      <button *ngIf="!myToken" ion-button  (click)="goLog()">تسجيل دخول</button>\n  </div>\n  <ion-list  *ngIf="myCars[0]">\n    <ion-list-header>السيارات الخاصة بك</ion-list-header>\n    <ion-item *ngFor="let car of myCars">\n      <ion-thumbnail (click)="proceed(car)" item-start>\n        <img *ngIf="car.make.logo" src="{{car.make.logo}}">\n        <img *ngIf="!car.make.logo" src="assets/imgs/not.png">\n      </ion-thumbnail>\n      <h2 ion-text color="primary" (click)="proceed(car)" >{{car.make.title}}</h2>\n      <p>{{car.model.title}} / {{car.year}}</p>\n      <button ion-button color="danger" (click)="removeItem(car)"  clear item-end>إزالة</button>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/mygarage/mygarage.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__["a" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _h || Object])
    ], MygaragePage);
    return MygaragePage;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=mygarage.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__orderdetails_orderdetails__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__myorders_myorders__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_auth__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__favoriteparts_favoriteparts__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__addresses_addresses__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__editprofile_editprofile__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, modalCtrl, app, storage, http, events, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.app = app;
        this.storage = storage;
        this.http = http;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.choice = "fav";
        this.myUser = {};
        this.choice = "fav";
    }
    ProfilePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        console.log('entered');
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                _this.u = val;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    _this.orders = data.orders;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                });
            }
            else {
                loading.dismiss();
            }
        });
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.goDetails = function (order) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__orderdetails_orderdetails__["a" /* OrderdetailsPage */], { order: order });
    };
    ProfilePage.prototype.openCart = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]);
        modal.onDidDismiss(function (data) {
            _this.myCart = data;
            _this.cartLength = _this.myCart.length;
        });
        modal.present();
    };
    ProfilePage.prototype.goOrders = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__myorders_myorders__["a" /* MyordersPage */], { orders: this.orders });
    };
    ProfilePage.prototype.goFav = function (f) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__favoriteparts_favoriteparts__["a" /* FavoritepartsPage */], { favs: f });
    };
    ProfilePage.prototype.logOut = function () {
        var _this = this;
        this.storage.clear().then(function (val) {
            _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__auth_auth__["a" /* AuthPage */]);
        });
    };
    ProfilePage.prototype.openContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__contact_contact__["a" /* ContactPage */]);
    };
    ProfilePage.prototype.goAddress = function (ad) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__addresses_addresses__["a" /* AddressesPage */], { adds: ad });
    };
    ProfilePage.prototype.editProfile = function () {
        var modalProf = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__editprofile_editprofile__["a" /* EditprofilePage */], { user: this.u });
        modalProf.onDidDismiss(function (data) {
            console.log(data);
        });
        modalProf.present();
    };
    ProfilePage.prototype.goLog = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_14__login_login__["a" /* LoginPage */]);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/profile/profile.html"*/'<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>\n      <img class="logoBar" src="assets/imgs/tanboora.png">\n    </ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons start>\n      <button (click)="openContact()" ion-button icon-only color="royal">\n        <ion-icon name="call">          \n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <ion-row padding>\n    <ion-col *ngIf="!myUser.id" text-center>\n        <img class="supImg" src="assets/imgs/profile.png"><br>\n        <button ion-button (click)="goLog()">تسجيل دخول</button>\n    </ion-col>\n    <ion-col text-center *ngIf="myUser.id" >\n      <img class="supImg" *ngIf="myUser.image" src="{{myUser.image}}">\n      <img class="supImg" *ngIf="!myUser.image" src="assets/imgs/profile.png">\n      <br>\n      <b>{{myUser.name}}\n        <ion-icon (click)="editProfile()" name="md-create" color="primary"></ion-icon>\n      </b>\n    </ion-col>\n  </ion-row>\n\n  <ion-list *ngIf="myUser.id">\n    <ion-item (click)="goOrders()">\n      <ion-icon name="cart"  color="primary" item-start></ion-icon>\n      طلباتي\n    </ion-item>\n    <ion-item (click)="goFav(myUser.fav_products)">\n      <ion-icon name="heart" color="primary" item-start></ion-icon>\n      أجزاء المفضلة\n    </ion-item>\n    <ion-item (click)="goAddress(myUser.addresses)">\n      <ion-icon name="book" color="primary"  item-start></ion-icon>\n      عناوين مسجلة\n    </ion-item>\n    <ion-item (click)="logOut()">\n      <ion-icon  name="log-out" color="primary" item-start></ion-icon>\n      الخروج\n    </ion-item>\n  </ion-list>\n\n  \n\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["b" /* Storage */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_11__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__angular_http__["a" /* Http */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _h || Object])
    ], ProfilePage);
    return ProfilePage;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyordersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__orderdetails_orderdetails__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MyordersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyordersPage = (function () {
    function MyordersPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.orders = this.navParams.get('orders');
    }
    MyordersPage.prototype.goDetails = function (order) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__orderdetails_orderdetails__["a" /* OrderdetailsPage */], { order: order });
    };
    MyordersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyordersPage');
    };
    MyordersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-myorders',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/myorders/myorders.html"*/'<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>طلباتي</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <ion-list >\n    <ion-item *ngFor="let order of orders" (click)="goDetails(order)">\n      <h2># {{order.id}}</h2>\n      <p>حالة الطلب </p>\n      <span ion-text color="{{order.status}}" item-end>{{order.status}}</span>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/myorders/myorders.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], MyordersPage);
    return MyordersPage;
}());

//# sourceMappingURL=myorders.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritepartsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the FavoritepartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FavoritepartsPage = (function () {
    function FavoritepartsPage(navCtrl, navParams, storage, http, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.myFavParts = [];
        this.myFavParts = [];
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val.id) {
                _this.myToken = val.id;
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myFavParts = data.fav_products;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                });
            }
        });
    }
    FavoritepartsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritepartsPage');
    };
    FavoritepartsPage.prototype.partDetails = function (p) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__["a" /* PartdetailsPage */], { part: p });
    };
    FavoritepartsPage.prototype.removeItem = function (item) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/remove_fav_product/?user_id=' + this.myToken + '&product_id=' + item.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myFavParts = data.fav_products;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    FavoritepartsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-favoriteparts',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/favoriteparts/favoriteparts.html"*/'<!--\n  Generated template for the FavoritepartsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>أجزاء المفضلة</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <div padding text-center *ngIf="!myFavParts[0]">\n    <ion-icon class="bigIcon" name="heart-outline"></ion-icon><br>\n    <p ion-text color="gray" text-center>لا توجد اي منتجات في المفضلة</p>\n  </div>\n    <ion-list *ngIf="myFavParts[0]">\n        <ion-item  *ngFor="let part of myFavParts" >\n          <ion-thumbnail  item-start (click)="partDetails(part)">\n            <img src="{{part.image}}">\n          </ion-thumbnail>\n          <h2 ion-text color="primary" (click)="partDetails(part)">{{part.title}}</h2>\n          <button ion-button color="danger" (click)="removeItem(part)" clear item-end>إزالة</button>\n        </ion-item>\n      </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/favoriteparts/favoriteparts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], FavoritepartsPage);
    return FavoritepartsPage;
}());

//# sourceMappingURL=favoriteparts.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plusaddress_plusaddress__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AddressesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddressesPage = (function () {
    function AddressesPage(navCtrl, navParams, http, loadingCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.myAddresses = [];
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val.id) {
                _this.myToken = val.id;
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myAddresses = data.addresses;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                });
            }
        });
    }
    AddressesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val.id) {
                _this.myToken = val.id;
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myAddresses = data.addresses;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                });
            }
        });
    };
    AddressesPage.prototype.address = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__plusaddress_plusaddress__["a" /* PlusaddressPage */]);
    };
    AddressesPage.prototype.removeItem = function (e) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/remove_address/?user_id=' + this.myToken + '&address_id=' + e.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            _this.myAddresses = data.addresses;
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    AddressesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-addresses',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/addresses/addresses.html"*/'\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title> العناوين المسجلة</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div padding text-center *ngIf="!myAddresses[0]">\n    <ion-icon class="bigIcon" name="bookmark"></ion-icon>\n    <br>\n    <p ion-text color="gray" text-center>لا توجد اي عناوين مسجلة</p>\n  </div>\n  <ion-list *ngIf="myAddresses[0]">\n    <ion-item *ngFor="let x of myAddresses">\n      <strong>{{x.area}}</strong>\n      <p>{{x.details}}</p>\n      <button ion-button color="danger" (click)="removeItem(x)" clear item-end>إزالة</button>\n    </ion-item>\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <div padding>\n    <button block ion-button (click)="address()">اضف عنوان</button>\n  </div>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/addresses/addresses.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], AddressesPage);
    return AddressesPage;
}());

//# sourceMappingURL=addresses.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditprofilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditprofilePage = (function () {
    function EditprofilePage(navCtrl, navParams, viewCtrl, storage, http, loadingCtrl, camera, transfer, file, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.storage = storage;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.toastCtrl = toastCtrl;
        this.u = {};
        this.fileTransfer = this.transfer.create();
        this.u = this.navParams.get('user');
        this.storage.get('user_info').then(function (val) {
            _this.user_info = val;
            console.log('user_info=', val);
        });
    }
    EditprofilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditprofilePage');
    };
    EditprofilePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.myInfo);
    };
    // edit() {
    //   this.viewCtrl.dismiss(this.myInfo);
    // }
    EditprofilePage.prototype.updateInfo = function (name, email, phone) {
        var _this = this;
        console.log(name, email, phone);
        this.storage.get('user_info').then(function (val) {
            _this.user_info = val;
            console.log(_this.user_info);
            _this.http.get('http://tanboora.bit68.com/update_profile/?user_id=' + _this.user_info.id + '&picture_id' + _this.picId + '&name=' + name + '&email=' + email + '&phone=' + phone).map(function (res) { return res.json(); }).subscribe(function (data) {
                _this.myInfo = data;
                _this.u = data;
                _this.storage.set('user_info', data);
                console.log('updated info', data);
                _this.dismiss();
            }, function (err) {
                console.log(err);
                var toast = _this.toastCtrl.create({
                    message: err,
                    duration: 2000
                });
                toast.present();
                _this.dismiss();
            });
        });
    };
    EditprofilePage.prototype.updatePic = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 300,
            targetHeight: 300,
        };
        this.camera.getPicture(options).then(function (file_uri) {
            console.log(file_uri);
            _this.myPic = file_uri;
            var transfer_options = {
                fileKey: 'file',
                fileName: 'name.jpeg',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            };
            var loading = _this.loadingCtrl.create({
                content: ''
            });
            loading.present();
            _this.fileTransfer.upload(file_uri, 'http://tanboora.bit68.com/file_transfer/', transfer_options)
                .then(function (data) {
                console.log('transfer success', JSON.parse(data.response));
                _this.picId = JSON.parse(data.response).id;
                _this.http.get('http://tanboora.bit68.com/update_profile/?user_id=' + _this.user_info.id + '&picture_id=' + _this.picId).map(function (res) { return res.json(); }).subscribe(function (data) {
                    loading.dismiss();
                    console.log(data);
                    console.log('updated info', data);
                    _this.u = data;
                    // this.updateInfo(this.name,this.email,this.phone);
                }, function (err) {
                    var toast = _this.toastCtrl.create({
                        message: err,
                        duration: 2000
                    });
                    toast.present();
                    console.log(err);
                });
                // alert('transfer success' + data)
            }, function (err) {
                var toastTransfer = _this.toastCtrl.create({
                    message: err,
                    duration: 2000
                });
                toastTransfer.present();
                console.log('transfer err', err);
                // alert('transfer err' + err);
            });
        }, function (err) {
            var toastGetPic = _this.toastCtrl.create({
                message: err,
                duration: 2000
            });
            toastGetPic.present();
            console.log('get picture', err);
            // alert('get Picture err'+ err);
            // Handle error
        });
    };
    EditprofilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-editprofile',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/editprofile/editprofile.html"*/'<!--\n  Generated template for the EditprofilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="caramel">\n\n  <ion-navbar>\n    <ion-title> تعديل الحساب</ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="dismiss()" ion-button icon-only color="royal">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n<div padding text-center>\n    <img (click)="updatePic()" class="profImg" *ngIf="u.image" src="{{u.image}}">\n    <img (click)="updatePic()" class="profImg" *ngIf="!u.image" src="assets/imgs/profile.png">\n    <br>\n</div>\n<ion-list>\n  <ion-item>\n    <ion-label ion-text color="light" stacked>الاسم</ion-label>\n    <ion-input [(ngModel)]="name" value="{{u.name}}"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label ion-text color="light" stacked>البريد الالكتروني</ion-label>\n    <ion-input [(ngModel)]="email" value="{{u.email}}"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label ion-text color="light" stacked>رقم التليفون</ion-label>\n    <ion-input [(ngModel)]="phone" value="{{u.phone}}"></ion-input>\n  </ion-item>\n  <div padding>\n    <button ion-button block (click)="updateInfo(name,email,phone)">تعديل</button>\n  </div>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/editprofile/editprofile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], EditprofilePage);
    return EditprofilePage;
}());

//# sourceMappingURL=editprofile.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__supplierform_supplierform__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__about_about__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__terms_terms__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__review_review__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













/**
 * Generated class for the MorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MorePage = (function () {
    function MorePage(navCtrl, navParams, modalCtrl, loadingCtrl, http, storage, toast, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.toast = toast;
        this.events = events;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                    var toast = _this.toast.create({
                        message: err,
                        duration: 2000
                    });
                    toast.present();
                });
            }
        });
    }
    MorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MorePage');
    };
    MorePage.prototype.goContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */]);
    };
    MorePage.prototype.goSettings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__settings_settings__["a" /* SettingsPage */]);
    };
    MorePage.prototype.goTerms = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__terms_terms__["a" /* TermsPage */]);
    };
    MorePage.prototype.goSupplierForm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__supplierform_supplierform__["a" /* SupplierformPage */]);
    };
    MorePage.prototype.goAbout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__about_about__["a" /* AboutPage */]);
    };
    MorePage.prototype.openCart = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__cart_cart__["a" /* CartPage */]);
        modal.onDidDismiss(function (data) {
            _this.myCart = data;
            _this.cartLength = _this.myCart.length;
        });
        modal.present();
    };
    MorePage.prototype.openContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */]);
    };
    MorePage.prototype.goReview = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__review_review__["a" /* ReviewPage */]);
    };
    MorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-more',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/more/more.html"*/'<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>\n      <img class="logoBar" src="assets/imgs/tanboora.png">\n    </ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons start>\n      <button (click)="openContact()" ion-button icon-only color="royal">\n        <ion-icon name="call">          \n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list>\n    <ion-item>\n      <ion-label><ion-icon  color="primary" name="notifications" item-start></ion-icon><span>إخطارات</span></ion-label>\n      <ion-toggle checked="true"></ion-toggle>\n    </ion-item>\n    <ion-item (click)="goContact()">\n      <ion-icon ion-text color="primary" name="pin" item-start></ion-icon>\n      اتصل بنا\n    </ion-item>\n    <ion-item>\n      <ion-icon ion-text color="primary" name="share" item-start></ion-icon>\n      مشاركة التطبيق\n    </ion-item>\n    <ion-item>\n      <ion-icon ion-text color="primary" name="star" item-start></ion-icon>\n      تقييم طنبورة\n    </ion-item>\n    <ion-item>\n      <ion-icon color="primary" name="" item-start>v</ion-icon>\n      الإصدار\n      <span text-center item-end ion-text color="gray">v 1.0.1</span>\n    </ion-item>\n    <ion-item (click)="goAbout()">\n      <ion-icon ion-text color="primary" name="information-circle" item-start></ion-icon>\n      عن طنبورة\n    </ion-item>\n    <ion-item (click)="goTerms()">\n      <ion-icon ion-text color="primary" name="document" item-start></ion-icon>\n       الشروط و الاحكام\n    </ion-item>\n  </ion-list>\n</ion-content>\n<ion-footer>\n\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/more/more.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_modal_modal_controller__["a" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _h || Object])
    ], MorePage);
    return MorePage;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=more.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SettingsPage = (function () {
    function SettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/settings/settings.html"*/'<!--\n  Generated template for the SettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>إعدادات</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupplierformPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_toast_toast_controller__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SupplierformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SupplierformPage = (function () {
    function SupplierformPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    SupplierformPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupplierformPage');
    };
    SupplierformPage.prototype.submit = function () {
        var toast = this.toastCtrl.create({
            message: 'Your informations have been sent ! waiting for review',
            duration: 3000
        });
        toast.present();
    };
    SupplierformPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-supplierform',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/supplierform/supplierform.html"*/'<!--\n  Generated template for the SupplierformPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>نموذج المورد</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list>\n    <ion-item>\n      <ion-label stacked color="primary">اسم</ion-label>\n      <ion-input></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked color="primary">عنوان</ion-label>\n      <ion-input></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-item>\n          <ion-label stacked color="primary">\n            العلامات التجارية</ion-label>\n          <ion-select interface="action-sheet" >\n            <ion-option>BMW</ion-option>\n            <ion-option>AUDI</ion-option>\n            <ion-option>KIA</ion-option>\n          </ion-select>\n        </ion-item>\n      <ion-label stacked color="primary">نموذج</ion-label>\n      <ion-select interface="action-sheet" >\n        <ion-option>Model 1</ion-option>\n        <ion-option>Model 2</ion-option>\n        <ion-option>Model 3</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n        <ion-label stacked color="primary">أجزاء</ion-label>\n        <ion-select interface="action-sheet" >\n          <ion-option>Part 1</ion-option>\n          <ion-option>Part 2</ion-option>\n          <ion-option>Part 3</ion-option>\n        </ion-select>\n      </ion-item>\n    \n  </ion-list>\n</ion-content>\n<ion-footer padding>\n  <button block ion-button color="primary" (click)="submit()">تطبيق</button>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/supplierform/supplierform.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_toast_toast_controller__["a" /* ToastController */]])
    ], SupplierformPage);
    return SupplierformPage;
}());

//# sourceMappingURL=supplierform.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TermsPage = (function () {
    function TermsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TermsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TermsPage');
    };
    TermsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-terms',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/terms/terms.html"*/'<!--\n  Generated template for the TermsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>الشروط و الأحكام</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <p>البنود والشروط هي مجموعة من القواعد والإرشادات التي يجب على المستخدم الموافقة عليها من أجل استخدام موقعك على الويب أو تطبيقك للجوال. وهو بمثابة عقد قانوني بينك (الشركة) الذي لديه موقع أو التطبيق المحمول والمستخدم الذي الوصول إلى موقع الويب الخاص بك والتطبيق المحمول.\n\n    والامر متروك لكم لتعيين القواعد والإرشادات التي يجب على المستخدم الموافقة عليها. يمكنك التفكير في اتفاقية البنود والشروط باعتبارها الاتفاقية القانونية التي تحافظ فيها على حقوقك في استبعاد المستخدمين من تطبيقك في حالة إساءة استخدام تطبيقك، وفي الأماكن التي تحافظ فيها على حقوقك القانونية ضد مدمني التطبيقات المحتملين، وما إلى ذلك.\n    \n    كما تعرف الشروط والأحكام أيضا ب "شروط الخدمة" أو "شروط الاستخدام".\n    \n    يمكن استخدام هذا النوع من الاتفاقية القانونية لكل من موقعك الإلكتروني وتطبيقك للجوال. ليس مطلوبا (من غير المستحسن فعليا) الحصول على اتفاقيات وشروط منفصلة: واحدة لموقع الويب الخاص بك والآخر لتطبيقك للجوال.</p>\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/terms/terms.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], TermsPage);
    return TermsPage;
}());

//# sourceMappingURL=terms.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__parts_parts__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(navCtrl, navParams, modalCtrl, loadingCtrl, http, storage, app, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.app = app;
        this.events = events;
        this.myModels = [];
        this.mods = [];
        this.myYear = "2018";
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myToken = val.id;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                }, function (err) {
                    console.log(err);
                });
            }
        });
        //brands
        this.http.get('http://tanboora.bit68.com/car_makes/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.brands = data;
        }, function (err) {
            console.log(err);
        });
        //categories
        this.http.get('http://tanboora.bit68.com/categories/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.categories = data;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage.prototype.proceed = function (b, m, y, cat) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/search/?user_id=' + this.myToken + '&make_id=' + b + '&model_id=' + m).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__parts_parts__["a" /* PartsPage */], {
                data: data,
                cat: cat
            });
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    SearchPage.prototype.openCart = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
        modal.onDidDismiss(function (data) {
            _this.myCart = data;
            _this.cartLength = _this.myCart.length;
        });
        modal.present();
    };
    SearchPage.prototype.openContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_contact__["a" /* ContactPage */]);
    };
    SearchPage.prototype.updateBrand = function (e) {
        console.log(e);
        var carModel = e;
        this.mods = this.brands.filter(function (brand) {
            return brand.id == carModel;
        });
        console.log(this.mods);
        this.myModels = this.mods[0].models;
        console.log(this.myModels);
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title><img class="logoBar" src="assets/imgs/tanboora.png"></ion-title>\n    <ion-buttons dir="ltr" end>\n        <button (click)="openCart()" ion-button icon-only color="royal">\n          <ion-icon name="cart">\n            <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n          </ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-buttons start>\n        <button (click)="openContact()" ion-button icon-only color="royal">\n          <ion-icon name="call">          \n          </ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n    <ion-list no-lines class="form">\n        <ion-item class="transparent">\n          <!-- <ion-label>Brand</ion-label> -->\n          <ion-select class="whiteBg" block interface="action-sheet" [(ngModel)]="myBrand" (ngModelChange)="updateBrand(myBrand)">\n            <ion-option ion-text color="light" selected>الماركة</ion-option>\n            <ion-option ion-text color="light" *ngFor="let brand of brands" value="{{brand.id}}">{{brand.title}}</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item class="transparent">\n          <!-- <ion-label>Model</ion-label> -->\n          <ion-select class="whiteBg" interface="action-sheet" block [(ngModel)]="myModel">\n              <ion-option ion-text color="light" selected>موديل</ion-option>\n            <ion-option ion-text color="light" *ngFor="let model of myModels" value="{{model.id}}">{{model.title}}</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item class="transparent">\n          <!-- <ion-label>Year</ion-label> -->\n          <ion-datetime class="whiteBg" displayFormat="YYYY" pickerFormat="YYYY" [(ngModel)]="myYear" min="1981"></ion-datetime>\n        </ion-item>\n        <ion-item class="transparent">\n            <!-- <ion-label>Brand</ion-label> -->\n            <ion-select class="whiteBg" block interface="action-sheet" [(ngModel)]="myCat">\n              <ion-option ion-text color="light" selected>أجزاء</ion-option>\n              <ion-option ion-text color="light" *ngFor="let cat of categories" value="{{cat.id}}">{{cat.title}}</ion-option>\n            </ion-select>\n          </ion-item>\n        <br>\n        <div padding>\n          <button  ion-button block color="primary" (click)="proceed(myBrand,myModel,myYear,myCat)">بحث</button>\n        </div>\n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _h || Object])
    ], SearchPage);
    return SearchPage;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PartsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filter_filter__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PartsPage = (function () {
    function PartsPage(navCtrl, navParams, modalCtrl, storage, http, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        // this.items=[];
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        //categories
        this.http.get('http://tanboora.bit68.com/categories/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.categories = data;
        }, function (err) {
            console.log(err);
        });
        this.rate = 4;
        this.parts = this.navParams.get('data');
        this.myParts = this.navParams.get('cat');
        this.initializeItems();
        console.log(this.parts, this.myParts);
        //get info and cars
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myToken = val.id;
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCars = data.cars;
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                });
            }
        });
    }
    PartsPage_1 = PartsPage;
    PartsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PartsPage');
    };
    // getItems(e){
    //   console.log(e);
    // }
    PartsPage.prototype.initializeItems = function () {
        this.items = this.parts;
    };
    PartsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    PartsPage.prototype.updateChoice = function (e) {
        this.initializeItems();
        console.log(e);
        var carCategory = e;
        this.itemsTemp = this.items.filter(function (item) { return item.category_id == carCategory; });
        this.items = this.itemsTemp;
        console.log(this.itemsTemp);
    };
    PartsPage.prototype.goDetails = function (p) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__["a" /* PartdetailsPage */], { part: p });
    };
    PartsPage.prototype.goFilter = function () {
        var filter = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__filter_filter__["a" /* FilterPage */]);
        filter.present();
    };
    PartsPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(PartsPage_1);
        modal.present();
    };
    PartsPage = PartsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-parts',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/parts/parts.html"*/'<!--\n  Generated template for the PartsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title *ngIf="brand" color="black">{{brand}} {{model}}</ion-title>\n    <ion-title *ngIf="!brand" color="black">كل القطع</ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  \n  <ion-searchbar placeholder="بحث برقم القطعة او اسم الموديل" (ionInput)="getItems($event)"></ion-searchbar>\n  <ion-select [(ngModel)]="myParts" interface="action-sheet" (ngModelChange)="updateChoice(myParts)">\n      <ion-option ion-text color="light" selected>اختيار الأجزاء الخاصة بك</ion-option>\n      <ion-option ion-text color="light" *ngFor="let cat of categories" value="{{cat.id}}">{{cat.title}}</ion-option>\n    </ion-select>\n  <ion-grid>\n      <ion-list>\n        <div *ngIf="!items[0]" padding text-center class="whiteBg">\n          <ion-icon class="bigIcon" name="ios-information-circle-outline">\n          </ion-icon>\n          <br>\n          <p ion-text color="gray">لا توجد منتجات</p>\n        </div>\n        <ion-item *ngFor="let part of items" (click)="goDetails(part)">\n            <ion-thumbnail item-start>\n                <img src="{{part.image}}">\n              </ion-thumbnail>\n              <h2>{{part.title}}</h2>\n              <p>{{part.price}} ج.م</p>\n              <div>\n                  <div class="rating" text-right>\n                      <rating [(ngModel)]="rate" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star"\n                        nullable="false" (ngModelChange)="onModelChange($event)"></rating>\n                      <!-- <b ion-text color="dark">4/5</b><br> -->\n                    </div>\n              </div>\n              <span>بواسطة {{part.shop}}</span>\n              <p>{{part.make.title}} - {{part.model.title}}</p>\n        </ion-item>\n      </ion-list>\n\n  </ion-grid>\n  <!-- <ion-fab bottom right>\n    <button color="secondary" ion-fab mini (click)="goFilter()">\n      <ion-icon name="funnel"></ion-icon>\n    </button>\n  </ion-fab> -->\n\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/parts/parts.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_modal_modal_controller__["a" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _f || Object])
    ], PartsPage);
    return PartsPage;
    var PartsPage_1, _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=parts.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FilterPage = (function () {
    function FilterPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.filteredParts = [
            { name: 'Airbags' },
            { name: 'Brakes' },
            { name: 'Accessories' },
            { name: 'Engines' },
            { name: 'Cooling' },
        ];
    }
    FilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterPage');
    };
    FilterPage.prototype.submit = function (opt) {
        console.log(opt);
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-filter',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/filter/filter.html"*/'<!--\n  Generated template for the FilterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>منقي</ion-title>\n    <ion-buttons dir="ltr" end>\n        <button (click)="dismiss()" ion-button icon-only color="royal">\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list  radio-group multiple>\n    <ion-list-header>\n      اختيار أجزاء الخاص بك\n    </ion-list-header>\n\n    <ion-item *ngFor="let part of filteredParts">\n      <ion-label>{{part.name}}</ion-label>\n      <ion-checkbox [(ngModel)]="part.checked" color="secondary"></ion-checkbox>\n    </ion-item>\n  </ion-list>\n  <br>\n  <div padding>\n    <button ion-button block color="primary" (click)="submit(parts)">تطبيق</button>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/filter/filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 143:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 143;

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkout_checkout__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CartPage = (function () {
    function CartPage(navCtrl, navParams, viewCtrl, http, storage, loadingCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.myUser = {};
        this.myCartInfo = {};
        this.myCart = [];
        this.shipping = 100;
        //get user and cart
        this.storage.get('user_info').then(function (val) {
            var loading = _this.loadingCtrl.create({
                content: ''
            });
            loading.present();
            console.log(val);
            _this.myUser = val;
            _this.myToken = val.id;
            //get cart
            _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                _this.myCartInfo = data;
                _this.myCart = data.cart;
                _this.events.subscribe('item:added', function (data) {
                    console.log(data);
                    _this.myCart = data.cart;
                });
                _this.events.subscribe('item:removed', function (data) {
                    console.log(data);
                    _this.myCart = data.cart;
                });
                _this.subTotal = _this.myCartInfo.cart_total + _this.shipping;
                loading.dismiss();
            }, function (err) {
                console.log(err);
            });
        });
    }
    CartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CartPage');
    };
    CartPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.myCart);
    };
    CartPage.prototype.goCheckout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__checkout_checkout__["a" /* CheckoutPage */], {
            cart: this.myCart,
            total: this.myCartInfo.cart_total
        });
    };
    CartPage.prototype.removeItem = function (item) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/remove_from_cart/?user_id=' + this.myToken + '&cart_item_id=' + item.cart_item_id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.events.publish('item:removed', data);
            _this.myCartInfo = data;
            _this.myCart = data.cart;
            _this.subTotal = _this.myCartInfo.cart_total + _this.shipping;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    CartPage.prototype.goHome = function () {
        this.dismiss();
    };
    CartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-cart',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/cart/cart.html"*/'<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>السلة</ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="dismiss()" ion-button icon-only color="royal">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div padding text-center *ngIf="!myCart[0]">\n        <ion-icon class="bigIcon" name="cart"></ion-icon>\n        <br>\n        <p ion-text color="gray" text-center>لا توجد منتجات  </p>\n        <button ion-button  (click)="goHome()">تسوق الان</button>\n      </div>\n  <ion-grid>\n    <ion-row class="prod" *ngFor="let product of myCart">\n      <ion-col col-3>\n        <img class="cartImg" [style.background-image]="\'url(\' + product.product.image + \')\'">\n      </ion-col>\n      <ion-col col-8>\n        <h4 ion-text color="primary">{{product.product.title}}</h4>\n        <p ion-text color="dark">{{product.unit_price}} ج.م\n          <span ion-text color="secondary">({{product.quantity}})</span>\n        </p>\n        <span ion-text class="totPrice" >{{product.total_price}} ج.م</span>\n      </ion-col>\n      <ion-col col-1 item-end>\n        <ion-icon name="trash" (click)="removeItem(product)" color="danger"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <!-- <div class="separator"></div> -->\n\n  </ion-grid>\n\n</ion-content>\n<ion-footer *ngIf="myCart[0]">\n  <ion-row >\n    <ion-col col-6 text-center>\n      <h5 ion-text color="gray">حاصل الجمع :</h5>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h5 ion-text color="light">{{myCartInfo.cart_total}} جنيه مصري</h5>\n    </ion-col>\n\n    <ion-col col-6 text-center>\n      <h5 ion-text color="gray">الشحن :</h5>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h5 ion-text color="light">{{shipping}} جنيه مصري</h5>\n    </ion-col>\n    <div class="separator"></div>\n    <ion-col col-6 text-center>\n      <h3 ion-text color="dark"> مجموع :</h3>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h3 ion-text color="gray">{{subTotal}} جنيه مصري</h3>\n    </ion-col>\n    <ion-col col-12 padding>\n        <button ion-button block color="primary" (click)="goCheckout()">المتابعة إلى الدفع</button>\n    </ion-col>\n  </ion-row>\n  \n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/cart/cart.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _g || Object])
    ], CartPage);
    return CartPage;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		314,
		26
	],
	"../pages/addresses/addresses.module": [
		315,
		25
	],
	"../pages/auth/auth.module": [
		316,
		24
	],
	"../pages/cart/cart.module": [
		317,
		23
	],
	"../pages/checkout/checkout.module": [
		318,
		22
	],
	"../pages/editprofile/editprofile.module": [
		319,
		21
	],
	"../pages/favoriteparts/favoriteparts.module": [
		321,
		20
	],
	"../pages/filter/filter.module": [
		320,
		19
	],
	"../pages/login/login.module": [
		322,
		18
	],
	"../pages/more/more.module": [
		323,
		17
	],
	"../pages/mygarage/mygarage.module": [
		324,
		16
	],
	"../pages/myorders/myorders.module": [
		325,
		15
	],
	"../pages/orderdetails/orderdetails.module": [
		327,
		14
	],
	"../pages/partdetails/partdetails.module": [
		326,
		13
	],
	"../pages/parts/parts.module": [
		328,
		12
	],
	"../pages/payment/payment.module": [
		329,
		11
	],
	"../pages/plusaddress/plusaddress.module": [
		330,
		10
	],
	"../pages/profile/profile.module": [
		332,
		9
	],
	"../pages/register/register.module": [
		331,
		8
	],
	"../pages/review/review.module": [
		333,
		7
	],
	"../pages/reviewdetails/reviewdetails.module": [
		335,
		6
	],
	"../pages/search/search.module": [
		334,
		5
	],
	"../pages/settings/settings.module": [
		336,
		4
	],
	"../pages/splash/splash.module": [
		337,
		3
	],
	"../pages/supplier/supplier.module": [
		338,
		2
	],
	"../pages/supplierform/supplierform.module": [
		339,
		1
	],
	"../pages/terms/terms.module": [
		340,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 185;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__supplier_supplier__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navCtrl, modalCtrl, loadingCtrl, storage, http, events) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
        this.events = events;
        this.slides = [
            { src: 'assets/imgs/slide2.jpg' },
            { src: 'assets/imgs/slide3.jpg' }
        ];
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                    _this.events.subscribe('item:removed', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                    _this.cartLength = data.cart.length;
                }, function (err) {
                    console.log(err);
                });
            }
        });
        //featured_stores
        this.http.get('http://tanboora.bit68.com/featured_shops/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.featuredStores = data;
        }, function (err) {
            console.log(err);
        });
        //ads
        this.http.get('http://tanboora.bit68.com/ads_products/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.ads = data;
        }, function (err) {
            console.log(err);
        });
        //slider_images
        this.http.get('http://tanboora.bit68.com/home_slides/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.slides = data;
        }, function (err) {
            console.log(err);
        });
        //featured_products
        this.http.get('http://tanboora.bit68.com/featured_products/').map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.featuredProducts = data;
            loading.dismiss();
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    HomePage.prototype.openProd = function (p) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__partdetails_partdetails__["a" /* PartdetailsPage */], { part: p });
    };
    HomePage.prototype.openCart = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__cart_cart__["a" /* CartPage */]);
        modal.onDidDismiss(function (data) {
            _this.myCart = data;
            _this.cartLength = _this.myCart.length;
        });
        modal.present();
    };
    HomePage.prototype.openContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__contact_contact__["a" /* ContactPage */]);
    };
    HomePage.prototype.shopDetails = function (s) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__supplier_supplier__["a" /* SupplierPage */], { shop: s });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/home/home.html"*/'<ion-header >\n  <ion-navbar color="caramel">\n    <ion-title>\n      <img class="logoBar" src="assets/imgs/tanboora.png">\n    </ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons start>\n      <button (click)="openContact()" ion-button icon-only color="royal">\n        <ion-icon name="call">          \n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides pager="true">\n    <ion-slide *ngFor="let slide of slides">\n      <img src="{{slide.image}}">\n    </ion-slide>\n  </ion-slides>\n\n  <ion-grid>\n    <ion-row padding>\n      <h5 ion-text color="primary">منتجات مميزة</h5>\n    </ion-row>\n   \n    <ion-scroll class="myScroll" scrollX=\'true\'>\n      <div  *ngFor="let prod of featuredProducts" class="imageDiv" (click)="openProd(prod)">\n          <p dir="rtl">{{prod.title}}<br>\n            <span ion-text color="gray">{{prod.price}} جنيه مصري</span></p>\n          \n          <img src="{{prod.image}}"> \n        </div>\n    </ion-scroll>\n  </ion-grid>\n  <ion-row>\n      <ion-col padding>\n        <h5 ion-text color="primary">متاجر مميزة</h5>\n        <ion-list>\n          <ion-item *ngFor="let store of featuredStores" (click)="shopDetails(store)" >{{store.title}}</ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-grid>\n      <ion-row padding>\n        <h5 ion-text color="primary"> احتياجات سيارتي</h5>\n      </ion-row>\n     \n      <ion-scroll class="myScroll" scrollX=\'true\'>\n        <div  *ngFor="let prod of ads" class="imageDiv" (click)="openProd(prod)">\n            <p dir="rtl">{{prod.title}}<br>\n              <span ion-text color="gray">{{prod.price}} جنيه مصري</span></p>\n            \n            <img src="{{prod.image}}"> \n          </div>\n      </ion-scroll>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _f || Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = (function () {
    function PaymentPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    PaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymentPage.prototype.confirm = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */]);
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/payment/payment.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>دفع</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <p ion-text color="secondary">رقم بطاقة الائتمان</p>\n            </ion-col>\n        </ion-row>\n        <ion-row text-center>\n            <ion-col>\n                <ion-input placeholder="0000" type="tel" maxlength="4"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-input placeholder="0000" type="tel" maxlength="4"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-input placeholder="0000" type="tel" maxlength="4"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-input placeholder="0000" type="tel" maxlength="4"></ion-input>\n            </ion-col>\n        </ion-row>\n        <ion-row text-center>\n            <ion-col>\n                <ion-label stacked color="darkCyan">Expiry Month</ion-label>\n                <ion-input placeholder="MM" type="text" maxlength="2"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-label stacked color="darkCyan">تاريخ انتهاء الصلاحية</ion-label>\n                <ion-input placeholder="YY" type="text" maxlength="4"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-label stacked color="darkCyan">CCV</ion-label>\n                <ion-input placeholder="CCV" type="text" maxlength="4"></ion-input>\n            </ion-col>\n            <ion-col>\n                <ion-icon style="font-size:4.2em;" color="secondary" name="card"></ion-icon>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-input style="border:1px solid whitesmoke;padding:5px;" placeholder="Name on the card"></ion-input>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-card>\n</ion-content>\n<ion-footer padding>\n        <button block ion-button (click)="confirm()" color="primary">تأكيد</button>\n      </ion-footer>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/payment/payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SplashPage = (function () {
    function SplashPage(navCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        setTimeout(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__auth_auth__["a" /* AuthPage */]);
        }, 3000);
    }
    SplashPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SplashPage');
    };
    SplashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-splash',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/splash/splash.html"*/'\n\n\n<ion-content padding text-center>\n<img src="assets/imgs/splash-logo.png">\n</ion-content>\n<ion-footer>\n  <p text-center>Copyright @2018 <b>Tanboora</b></p>\n</ion-footer>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/splash/splash.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], SplashPage);
    return SplashPage;
}());

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(257);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_register_register__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_checkout_checkout__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_mygarage_mygarage__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_orderdetails_orderdetails__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_partdetails_partdetails__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_parts_parts__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_review_review__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_supplier_supplier__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ionic2_rating__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_payment_payment__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_more_more__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_supplierform_supplierform__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_search_search__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_terms_terms__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_myorders_myorders__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_favoriteparts_favoriteparts__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_splash_splash__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_addresses_addresses__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_reviewdetails_reviewdetails__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_plusaddress_plusaddress__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_editprofile_editprofile__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_facebook__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_mycart_mycart__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_camera__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_file_transfer__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__ionic_native_file__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__["a" /* AuthPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_mygarage_mygarage__["a" /* MygaragePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_orderdetails_orderdetails__["a" /* OrderdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_partdetails_partdetails__["a" /* PartdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_parts_parts__["a" /* PartsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_review_review__["a" /* ReviewPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_supplier_supplier__["a" /* SupplierPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_more_more__["a" /* MorePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_supplierform_supplierform__["a" /* SupplierformPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_myorders_myorders__["a" /* MyordersPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_favoriteparts_favoriteparts__["a" /* FavoritepartsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_splash_splash__["a" /* SplashPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_addresses_addresses__["a" /* AddressesPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_reviewdetails_reviewdetails__["a" /* ReviewdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_plusaddress_plusaddress__["a" /* PlusaddressPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_editprofile_editprofile__["a" /* EditprofilePage */],
                __WEBPACK_IMPORTED_MODULE_40__components_mycart_mycart__["a" /* MycartComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_37__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addresses/addresses.module#AddressesPageModule', name: 'AddressesPage', segment: 'addresses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/auth/auth.module#AuthPageModule', name: 'AuthPage', segment: 'auth', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cart/cart.module#CartPageModule', name: 'CartPage', segment: 'cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/checkout/checkout.module#CheckoutPageModule', name: 'CheckoutPage', segment: 'checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editprofile/editprofile.module#EditprofilePageModule', name: 'EditprofilePage', segment: 'editprofile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter/filter.module#FilterPageModule', name: 'FilterPage', segment: 'filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favoriteparts/favoriteparts.module#FavoritepartsPageModule', name: 'FavoritepartsPage', segment: 'favoriteparts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/more/more.module#MorePageModule', name: 'MorePage', segment: 'more', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mygarage/mygarage.module#MygaragePageModule', name: 'MygaragePage', segment: 'mygarage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/myorders/myorders.module#MyordersPageModule', name: 'MyordersPage', segment: 'myorders', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/partdetails/partdetails.module#PartdetailsPageModule', name: 'PartdetailsPage', segment: 'partdetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orderdetails/orderdetails.module#OrderdetailsPageModule', name: 'OrderdetailsPage', segment: 'orderdetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/parts/parts.module#PartsPageModule', name: 'PartsPage', segment: 'parts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/plusaddress/plusaddress.module#PlusaddressPageModule', name: 'PlusaddressPage', segment: 'plusaddress', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/review/review.module#ReviewPageModule', name: 'ReviewPage', segment: 'review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reviewdetails/reviewdetails.module#ReviewdetailsPageModule', name: 'ReviewdetailsPage', segment: 'reviewdetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash/splash.module#SplashPageModule', name: 'SplashPage', segment: 'splash', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/supplier/supplier.module#SupplierPageModule', name: 'SupplierPage', segment: 'supplier', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/supplierform/supplierform.module#SupplierformPageModule', name: 'SupplierformPage', segment: 'supplierform', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/terms/terms.module#TermsPageModule', name: 'TermsPage', segment: 'terms', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_23_ionic2_rating__["a" /* Ionic2RatingModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__["a" /* AuthPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_mygarage_mygarage__["a" /* MygaragePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_orderdetails_orderdetails__["a" /* OrderdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_partdetails_partdetails__["a" /* PartdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_parts_parts__["a" /* PartsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_review_review__["a" /* ReviewPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_supplier_supplier__["a" /* SupplierPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_more_more__["a" /* MorePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_supplierform_supplierform__["a" /* SupplierformPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_myorders_myorders__["a" /* MyordersPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_favoriteparts_favoriteparts__["a" /* FavoritepartsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_splash_splash__["a" /* SplashPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_addresses_addresses__["a" /* AddressesPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_reviewdetails_reviewdetails__["a" /* ReviewdetailsPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_plusaddress_plusaddress__["a" /* PlusaddressPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_editprofile_editprofile__["a" /* EditprofilePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_43__ionic_native_file__["a" /* File */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, config) {
        var _this = this;
        this.config = config;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            _this.config.set('backButtonText', "");
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Config */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MycartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_checkout_checkout__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MycartComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MycartComponent = (function () {
    function MycartComponent(navCtrl, navParams, viewCtrl, http, storage, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.myUser = {};
        this.myCartInfo = {};
        this.myCart = [];
        this.shipping = 100;
        //get user and cart
        this.storage.get('user_info').then(function (val) {
            var loading = _this.loadingCtrl.create({
                content: ''
            });
            loading.present();
            console.log(val);
            _this.myUser = val;
            _this.myToken = val.id;
            //get cart
            _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                _this.myCartInfo = data;
                _this.myCart = data.cart;
                _this.subTotal = _this.myCartInfo.cart_total + _this.shipping;
                loading.dismiss();
            }, function (err) {
                console.log(err);
            });
        });
    }
    MycartComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MycartComponent.prototype.goCheckout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_checkout_checkout__["a" /* CheckoutPage */], {
            cart: this.myCart,
            total: this.subTotal
        });
    };
    MycartComponent.prototype.removeItem = function (item) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/remove_from_cart/?user_id=' + this.myToken + '&product_id=' + item.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myCartInfo = data;
            _this.myCart = data.cart;
            _this.subTotal = _this.myCartInfo.cart_total + _this.shipping;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    MycartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'mycart',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/components/mycart/mycart.html"*/'<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>السلة</ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="dismiss()" ion-button icon-only color="royal">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-grid>\n    <ion-row class="prod" *ngFor="let product of myCart">\n      <ion-col col-3>\n        <img class="cartImg" [style.background-image]="\'url(\' + product.image + \')\'">\n      </ion-col>\n      <ion-col col-8>\n        <h4 ion-text color="primary">{{product.title}}</h4>\n        <p ion-text color="dark">{{product.price}} ج.م\n          <span ion-text color="gray">(1)</span>\n        </p>\n      </ion-col>\n      <ion-col col-1 item-end>\n        <ion-icon name="trash" (click)="removeItem(product)" color="danger"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <!-- <div class="separator"></div> -->\n\n  </ion-grid>\n\n</ion-content>\n<ion-footer >\n  <ion-row >\n    <ion-col col-6 text-center>\n      <h5 ion-text color="gray">حاصل الجمع :</h5>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h5 ion-text color="light">{{myCartInfo.cart_total}} جنيه مصري</h5>\n    </ion-col>\n\n    <ion-col col-6 text-center>\n      <h5 ion-text color="gray">الشحن :</h5>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h5 ion-text color="light">{{shipping}} جنيه مصري</h5>\n    </ion-col>\n    <div class="separator"></div>\n    <ion-col col-6 text-center>\n      <h3 ion-text color="dark"> مجموع :</h3>\n    </ion-col>\n    <ion-col col-6 text-right>\n      <h3 ion-text color="gray">{{subTotal}} جنيه مصري</h3>\n    </ion-col>\n    <ion-col col-12 padding>\n        <button ion-button block color="primary" (click)="goCheckout()">المتابعة إلى الدفع</button>\n    </ion-col>\n  </ion-row>\n  \n</ion-footer>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/components/mycart/mycart.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], MycartComponent);
    return MycartComponent;
}());

//# sourceMappingURL=mycart.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mygarage_mygarage__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__more_more__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__search_search__ = __webpack_require__(131);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__mygarage_mygarage__["a" /* MygaragePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__search_search__["a" /* SearchPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_4__more_more__["a" /* MorePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/tabs/tabs.html"*/'<ion-tabs color="caramel">\n  <ion-tab [root]="tab1Root" tabTitle="الرئيسية" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="سياراتي" tabIcon="car"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="بحث" tabIcon="search"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="حسابي" tabIcon="contact"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="أكثر" tabIcon="more"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactPage = (function () {
    function ContactPage(navCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
    }
    ContactPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
        modal.present();
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar color="caramel">\n    <ion-title>\n      اتصل\n    </ion-title>\n    <!-- <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart"><ion-badge color="secondary">٣</ion-badge></ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item >\n      <ion-icon ion-text color="primary" name="chatboxes" item-start></ion-icon>\n      info@tanboora.com\n    </ion-item>\n    <ion-item >\n      <ion-icon ion-text color="primary" name="call" item-start></ion-icon>\n      +2xxxxxxxxxxx\n    </ion-item>\n    <ion-item>\n      <ion-icon ion-text color="primary" name="globe" item-start></ion-icon>\n      tanboora.com\n    </ion-item>\n    \n  </ion-list>\n</ion-content>\n\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PartdetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__supplier_supplier__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_toast_toast_controller__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__reviewdetails_reviewdetails__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the PartdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PartdetailsPage = (function () {
    function PartdetailsPage(navCtrl, navParams, modalCtrl, toastCtrl, loadingCtrl, http, storage, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.events = events;
        this.rate = 4;
        this.ratee = 4;
        this.myInfo = {};
        this.myCart = {};
        //get part details
        this.part = this.navParams.get('part');
        this.rate = 4;
        //get user_id
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            _this.myInfo = val;
            if (val.id) {
                _this.myToken = val.id;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCart = data;
                    _this.cartLength = data.cart.length;
                    _this.events.subscribe('item:added', function (data) {
                        _this.myCart = data.cart;
                        _this.cartLength = data.cart.length;
                    });
                }, function (err) {
                    console.log(err);
                });
            }
        });
    }
    PartdetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PartdetailsPage');
    };
    PartdetailsPage.prototype.viewSupplier = function (e) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__supplier_supplier__["a" /* SupplierPage */], { shop_id: e });
    };
    PartdetailsPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]);
        modal.present();
    };
    PartdetailsPage.prototype.like = function (p) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_fav_product/?user_id=' + this.myToken + '&product_id=' + p.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            var added = _this.toastCtrl.create({
                message: "تم الاضافة للمفضلة",
                duration: 2000
            });
            added.present();
        }, function (err) {
            console.log(err);
        });
    };
    PartdetailsPage.prototype.goDetails = function (e) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__reviewdetails_reviewdetails__["a" /* ReviewdetailsPage */], { part: e });
    };
    PartdetailsPage.prototype.addToCart = function (p) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_to_cart/?user_id=' + this.myToken + '&product_id=' + p.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            _this.events.publish('item:added', data);
            var added = _this.toastCtrl.create({
                message: "تم الاضافة للسلة",
                duration: 2000
            });
            added.present();
        }, function (err) {
            console.log(err);
        });
    };
    PartdetailsPage.prototype.onModelChange = function (e) {
        console.log(e);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */])
    ], PartdetailsPage.prototype, "slides", void 0);
    PartdetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-partdetails',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/partdetails/partdetails.html"*/'<!--\n  Generated template for the PartdetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>{{part.title}}</ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-slides pager>\n      <ion-slide autoplay="5000" loop="true" speed="500" *ngFor="let x of [1,2,3]">\n        <img class="prodImg" [style.background-image]="\'url(\' + part.image + \')\'">\n      </ion-slide>\n    </ion-slides>\n  </ion-row>\n  <br>\n  <div class="product">\n  <div (click)="goDetails(part)" class="rating" padding text-center>\n    <rating [(ngModel)]="ratee" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star"\n      nullable="false" (ngModelChange)="onModelChange($event)"></rating>({{part.reviews.length}} تقييمات)\n  </div>\n</div>\n  <!-- <ion-row padding>\n    <ion-col text-center>\n      <img class="prodImg" src="{{part.src}}">\n    </ion-col>\n  </ion-row> -->\n  <ion-row>\n    <ion-col col-8 ion-text color="primary">\n      <b>رقم القطعة:</b>\n    </ion-col>\n    <ion-col ion-text color="gray" col-4>{{part.title}}</ion-col>\n    <ion-col col-8 ion-text color="primary">\n      <b>الماركة :</b>\n    </ion-col>\n    <ion-col ion-text color="gray" col-4>{{part.make.title}}</ion-col>\n    <ion-col col-8 ion-text color="primary">\n      <b>سيارة :</b>\n    </ion-col>\n    <ion-col ion-text color="gray" col-4>{{part.model.title}}</ion-col>\n    <ion-col col-12 ion-text color="primary">\n      <b>وصف :</b>\n    </ion-col>\n    <ion-col col-12 ion-text color="gray">{{part.description}}</ion-col>\n    <ion-col col-4 ion-text color="primary">\n      <b>المورد:</b>\n    </ion-col>\n    <ion-col (click)="viewSupplier(part.shop_id)" col-4 ion-text color="gray" text-left>\n      <b>{{part.shop}}</b>\n    </ion-col>\n    <ion-col col-4 ion-text color="gray">\n      <div class="store">\n        <div class="rating" text-right>\n          <rating [(ngModel)]="rate" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star"\n            nullable="false" (ngModelChange)="onModelChange($event)"></rating>\n          <!-- <b ion-text color="dark">4/5</b><br> -->\n        </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <!-- <ion-row class="whiteBg shadow text-center">\n    <ion-col>\n      <div class="rating" padding text-center>\n        <rating [(ngModel)]="ratee" readOnly="false" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star"\n          nullable="false" (ngModelChange)="onModelChange($event)"></rating>\n        <b ion-text color="dark">{{ratee}}/5</b>\n\n      </div>\n    </ion-col>\n  </ion-row> -->\n  <ion-row>\n    <ion-col>\n      <button ion-button block (click)="addToCart(part)" color="light">\n        <ion-icon name="cart"></ion-icon>\n      </button>\n    </ion-col>\n    <ion-col>\n      <button ion-button block (click)="like(part)" color="light">\n        <ion-icon name="heart"></ion-icon>\n      </button>\n    </ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/partdetails/partdetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_toast_toast_controller__["a" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], PartdetailsPage);
    return PartdetailsPage;
}());

//# sourceMappingURL=partdetails.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReviewPage = (function () {
    function ReviewPage(navCtrl, navParams, viewCtrl, storage, http, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.storage = storage;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.rate = 1;
        this.review = "";
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
            }
        });
        this.myShop = this.navParams.get('shop');
        this.myShopId = this.navParams.get('shop_id');
        this.myPartId = this.navParams.get('product_id');
        console.log(this.myShopId);
    }
    ReviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReviewPage');
    };
    ReviewPage.prototype.onModelChange = function (e) {
        console.log(e);
    };
    ReviewPage.prototype.submit = function (rate, review) {
        var _this = this;
        console.log(rate, review);
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_review/?user_id=' + this.myUser.id + '&shop_id=' + this.myShopId + '&review_text=' + review + '&review_rate=' + rate + '&product_id=' + this.myPartId).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myData = data;
            loading.dismiss();
            _this.dismiss();
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    ReviewPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.myData);
    };
    ReviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-review',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/review/review.html"*/'<!--\n  Generated template for the ReviewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title> تقييم</ion-title>\n    <ion-buttons dir="ltr" end>\n        <button (click)="dismiss()" ion-button icon-only color="royal">\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="rating" padding text-center>\n    <rating [(ngModel)]="rate" readOnly="false"  max="5" emptyStarIconName="star-outline"\n     halfStarIconName="star-half"  starIconName="star"  nullable="false"\n     (ngModelChange)="onModelChange($event)"></rating>\n     <b ion-text color="dark">{{rate}}/5</b><br>\n     <div padding>\n     <ion-textarea [(ngModel)]="review" rows="5" placeholder="اكتب تعليق"></ion-textarea>\n    </div>\n  </div>\n</ion-content>\n<ion-footer>\n  <div padding>\n    <button (click)="submit(rate,review)" ion-button color="secondary" block>ارسال</button>\n  </div>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/review/review.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_view_controller__["a" /* ViewController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], ReviewPage);
    return ReviewPage;
}());

//# sourceMappingURL=review.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_facebook__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, http, storage, loadingCtrl, toast, fb) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.toast = toast;
        this.fb = fb;
        fb.getLoginStatus()
            .then(function (res) {
            console.log(res.status);
            if (res.status === "connect") {
                _this.isLoggedIn = true;
            }
            else {
                _this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log(e); });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function (e, p) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/login/?email=' + e + '&password=' + p).map(function (res) { return res.json(); }).subscribe(function (data) {
            loading.dismiss();
            console.log(data);
            if (data.name) {
                _this.myUser = data;
                _this.storage.set('user_info', data);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                var toast = _this.toast.create({
                    message: data._body,
                    duration: 2000
                });
                toast.present();
            }
        }, function (err) {
            loading.dismiss();
            var toast = _this.toast.create({
                message: err.statusText,
                duration: 2000
            });
            toast.present();
            console.log(err);
        });
    };
    LoginPage.prototype.goReg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.loginFB = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            loading.dismiss();
            if (res.status === "connected") {
                _this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.isLoggedIn = false;
            }
            console.log('Logged into Facebook!', res);
        })
            .catch(function (e) {
            loading.dismiss();
            console.log('Error logging into Facebook', e);
            var toast = _this.toast.create({
                message: e,
                duration: 2000
            });
            toast.present();
        });
    };
    LoginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.http.get('http://tanboora.bit68.com/login/?fb_token=' + userid + '&email=' + res.email + '&name=' + res.name + '&image=' + res.picture.data.url).map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                if (data.id) {
                    _this.storage.set('user_info', data).then(function (val) {
                        console.log('storage value', val);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
                    });
                }
                else {
                    var toast = _this.toast.create({
                        message: data._body,
                        duration: 2000
                    });
                    toast.present();
                }
            }, function (err) {
                console.log(err);
                var toast = _this.toast.create({
                    message: err,
                    duration: 2000
                });
                toast.present();
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>تسجيل الدخول</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div padding text-center>\n    <img src="assets/imgs/tanboora.png">\n    \n  </div>\n  <ion-list>\n    <ion-item>\n      <!-- <ion-label stacked color="primary">Email</ion-label> -->\n      <ion-input placeholder="البريد الالكتروني" [(ngModel)]="email"></ion-input>\n    </ion-item>\n    <ion-item>\n\n      <!-- <ion-label stacked color="primary">Password </ion-label> -->\n      <ion-input placeholder="كلمة المرور" [(ngModel)]="password" type="password"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label color="primary" class="smText">تذكرنى</ion-label>\n      <ion-checkbox></ion-checkbox>\n    </ion-item>\n    <br>\n    <button class="radBtn" block ion-button [disabled]="!email || !password" (click)="login(email,password)">تسجيل الدخول</button>\n    <p text-center ion-text color="gray">أو</p>\n    <button block ion-button class="fbBtn" (click)="loginFB()">تسجيل الدخول باستخدام الفيسبوك</button>\n  </ion-list>\n  <p text-center>لم يكن لديك حساب مع تانبورة؟ <b (click)="goReg()">سجل </b></p>\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_facebook__["a" /* Facebook */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__plusaddress_plusaddress__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CheckoutPage = (function () {
    function CheckoutPage(navCtrl, navParams, storage, http, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.showVisa = false;
        this.order = [];
        this.payment = "cash";
        this.shipping = 100;
        this.addresses = [];
        this.order = this.navParams.get('cart');
        this.total = this.navParams.get('total');
        this.subTotal = this.total + this.shipping;
    }
    CheckoutPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                //get cart
                _this.http.get('http://tanboora.bit68.com/user_info/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myUser = data;
                    _this.phone = data.phone;
                    _this.addresses = data.addresses;
                    _this.address = data.addresses[0].id;
                    _this.name = data.name;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                });
            }
        });
    };
    CheckoutPage.prototype.goPayment = function (tot, name, add, phone) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        console.log(tot, name, add, phone);
        this.http.get('http://tanboora.bit68.com/place_order/?user_id=' + this.myUser.id + '&name=' + name + '&address_id=' + add + '&phone=' + phone + '&total=' + tot).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            var order = _this.toastCtrl.create({
                message: "تم ارسال طلبك في انتظار التأكيد",
                duration: 2000
            });
            order.present();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    CheckoutPage.prototype.goAddress = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__plusaddress_plusaddress__["a" /* PlusaddressPage */]);
    };
    CheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-checkout',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/checkout/checkout.html"*/'<!--\n  Generated template for the CheckoutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="caramel">\n    <ion-title>الدفع\n\n      </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h3 ion-text color="primary">تفاصيل الدفع</h3><br>\n  <ion-row *ngFor="let prod of order">\n    <ion-col col-2>\n      <img src="{{prod.product.image}}">\n    </ion-col>\n    <ion-col col-9>\n      <h5 ion-text color="primary">{{prod.product.title}}</h5>\n      <p ion-text color="dark">{{prod.unit_price}} ج.م\n        <span ion-text color="secondary">({{prod.quantity}})</span>\n      </p>\n    </ion-col>\n    <ion-col col-1>\n      <ion-icon color="danger" name="trash"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <div class="separator"></div>\n  <ion-row>\n    <ion-col col-12 ion-text color="gray">حاصل الجمع  : {{total}} جنيه مصري</ion-col>\n    <ion-col col-12 ion-text color="gray">الشحن: {{this.shipping}} جنيه مصري</ion-col>\n    <ion-col col-12>\n      <b ion-text color="dark">المجموع : {{this.subTotal}} جنيه مصري</b>\n    </ion-col>\n  </ion-row>\n  <div class="separator"></div>\n\n  <h3 ion-text color="primary">معلومات المستلم</h3>\n  <br>\n  <ion-list>\n    <ion-item>\n      <ion-label ion-text stacked color="light">الأسم</ion-label>\n      <ion-input [(ngModel)]="name"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="addresses[0]">\n      <ion-label ion-text stacked color="light">العنوان</ion-label>\n      <ion-select [(ngModel)]="address" >\n        <ion-option *ngFor="let add of addresses " value="{{add.id}}">{{add.area}} - {{add.details}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item *ngIf="!addresses[0]">\n      \n      <button ion-button block (click)="goAddress()">أضف عنوان</button>\n    </ion-item>\n    <ion-item>\n      <ion-label ion-text stacked color="light">رقم التليفون</ion-label>\n      <ion-input [(ngModel)]="phone"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <div class="separator"></div>\n\n  <h3 ion-text color="primary">طريقة الدفع او السداد</h3><br>\n  <ion-list radio-group [(ngModel)]="payment">\n      <ion-item>\n        <ion-label>كاش عند الاستلام</ion-label>\n        <ion-radio color="secondary" value="cash" checked></ion-radio>\n      </ion-item>\n      <!-- <ion-item>\n        <ion-label>بطاقة ائتمان</ion-label>\n        <ion-radio color="secondary" value="credit"></ion-radio>\n      </ion-item> -->\n  </ion-list>\n</ion-content>\n<ion-footer padding>\n  <button ion-button block [disabled]="!name||!phone||!address" color="primary" (click)="goPayment(subTotal,name,address,phone)">اطلب</button>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/checkout/checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], CheckoutPage);
    return CheckoutPage;
}());

//# sourceMappingURL=checkout.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupplierPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__review_review__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__partdetails_partdetails__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SupplierPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SupplierPage = (function () {
    function SupplierPage(navCtrl, navParams, modalCtrl, loadingCtrl, http, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.reviews = [];
        this.shop = {};
        this.suppItems = [];
        this.temp = {};
        this.temp = this.navParams.get('shop');
        if (this.temp != undefined) {
            console.log(this.temp);
            this.shop = this.temp;
            this.suppItems = this.shop.products;
            this.reviews = this.shop.reviews;
        }
        else {
            this.shop_id = this.navParams.get('shop_id');
            this.http.get('http://tanboora.bit68.com/shop?shop_id=' + this.shop_id).map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                _this.shop = data;
                _this.suppItems = _this.shop.products;
                _this.reviews = _this.shop.reviews;
            }, function (err) {
                console.log(err);
            });
        }
        // console.log(this.navParams.get('shop'));
        // console.log(this.navParams.get('shop_id'));
        // if (this.navParams.get('shop')!=='undefined') {
        //   this.shop = this.navParams.get('shop');
        //   this.suppItems = this.shop.products;
        //   this.reviews = this.shop.reviews;
        // } else if(this.navParams.get('shop_id')!=='undefined') {
        //   this.shop_id=this.navParams.get('shop_id');
        //   this.http.get('http://tanboora.bit68.com/shop?shop_id=' + this.shop_id).map(res=>res.json()).subscribe((data:any)=>{
        //     this.shop=data;
        //     this.suppItems=this.shop.products;
        //     this.reviews = this.shop.reviews;
        //     console.log(data);
        //   },err=>{
        //     console.log(err);
        //   })
        // }else{
        //   console.log('err');
        // }
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                //get cart
                _this.http.get('http://tanboora.bit68.com/get_cart/?user_id=' + _this.myUser.id).map(function (res) { return res.json(); }).subscribe(function (data) {
                    console.log(data);
                    _this.myCartInfo = data;
                    _this.myCart = data.cart;
                    _this.cartLength = data.cart.length;
                    loading.dismiss();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                });
            }
        });
    }
    SupplierPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupplierPage');
    };
    SupplierPage.prototype.addReview = function () {
        var review = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__review_review__["a" /* ReviewPage */], { shop: this.shop });
        review.present();
    };
    SupplierPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]);
        modal.present();
    };
    SupplierPage.prototype.openProd = function (p) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__partdetails_partdetails__["a" /* PartdetailsPage */], { part: p });
    };
    SupplierPage.prototype.react = function (r) {
        var _this = this;
        console.log(r);
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_review_like/?user_id=' + this.myUser.id + '&review_id=' + r.id).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.reviews = data;
            loading.dismiss();
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    SupplierPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-supplier',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/supplier/supplier.html"*/'<!--\n  Generated template for the SupplierPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>المورد\n    </ion-title>\n    <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart">\n          <ion-badge color="secondary">{{this.cartLength}}</ion-badge>\n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row padding>\n    <ion-col text-center>\n      <img class="supImg" [style.background-image]="\'url(\' + shop.image + \')\'">\n      <br>\n      <b>{{shop.title}}</b>\n      <br>\n      <b ion-text color="secondary">{{shop.avg_rate}}/5</b>\n      <br>\n      <!-- <button ion-button small color="primary" (click)="addReview()">معدل المورد</button> -->\n    </ion-col>\n  </ion-row>\n  <!-- <ion-row>\n    <ion-col col-4 ion-text color="dark">البريد الإلكتروني</ion-col>\n    <ion-col ion-text color="gray" col-8>{{shop.email}}</ion-col>\n\n    <ion-col col-4 ion-text color="dark">عنوان</ion-col>\n    <ion-col col-8 ion-text color="gray">{{shop.address}}</ion-col>\n\n    <ion-col col-4 ion-text color="dark">هاتف</ion-col>\n    <ion-col col-8 ion-text color="gray">\n      {{shop.phone}}\n    </ion-col>\n  </ion-row> -->\n  <h3 ion-text color="primary">المنتجات</h3>\n  <div padding text-center class="whiteBg" *ngIf="!suppItems[0]">\n    <ion-icon class="bigIcon" name="ios-information-circle-outline"></ion-icon><br>\n    <p ion-text color="gray" text-center>لا توجد منتجات</p>\n  </div>\n  <ion-scroll *ngIf="suppItems[0]" class="myScroll" scrollX=\'true\'>\n    <div  *ngFor="let prod of suppItems" class="imageDiv" (click)="openProd(prod)">\n        <p dir="rtl">{{prod.title}}<br>\n          <span ion-text color="gray">{{prod.price}} جنيه مصري</span></p>\n        \n        <img src="{{prod.image}}"> \n      </div>\n  </ion-scroll>\n  <!-- <div class="wrapper">\n    <div class="scrolls">\n      <div  *ngFor="let prod of suppItems" class="imageDiv" (click)="openProd(prod)">\n        <p dir="rtl">{{prod.title}}<br>\n          <span ion-text color="gray">{{prod.price}} جنيه مصري</span></p>\n        \n        <img src="{{prod.image}}"> \n      </div>\n    </div>\n  </div> -->\n  <br>\n  <div>\n    <h3 ion-text color="primary">التعليقات</h3>\n    <div class="whiteBg" padding text-center *ngIf="!reviews">\n      <ion-icon class="bigIcon" name="text"></ion-icon><br>\n      <p ion-text color="gray" text-center>لا توجد تعليقات</p>\n    </div>\n    <div *ngIf="reviews">\n      <ion-card *ngFor="let review of reviews">\n        <ion-item >\n          <!-- <ion-avatar item-start>\n            <h5>{{}}</h5>\n          </ion-avatar> -->\n          <h2 ion-text color="primary">{{review.user_name}}</h2>\n          <ion-note item-end>\n            {{review.time}}\n          </ion-note>\n          <p>{{review.date}}</p>\n        </ion-item>\n        <ion-card-content>\n          <p>" {{review.text}} "</p>\n        </ion-card-content>\n  \n        <ion-row class="likes">\n          <ion-col>\n            <button (click)="react(review)" ion-button icon-left clear small>\n              <ion-icon color="secondary" name="thumbs-up" (click)="react(review)"></ion-icon>\n              <div ion-text color="secondary"> {{review.likes_count}} </div>\n            </button>\n          </ion-col>\n          \n        </ion-row>\n      </ion-card>\n    </div>\n    \n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/supplier/supplier.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]])
    ], SupplierPage);
    return SupplierPage;
}());

//# sourceMappingURL=supplier.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, http, storage, loadingCtrl, toast, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.toast = toast;
        this.fb = fb;
        this.myUser = {};
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.register = function (n, e, p, phone) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/register/?email=' + e + '&password=' + p + '&name=' + n + '&phone=' + phone).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            if (data.name) {
                _this.myUser = data;
                _this.storage.set('user_info', data);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                var toast = _this.toast.create({
                    message: data._body,
                    duration: 2000
                });
                toast.present();
            }
        }, function (err) {
            loading.dismiss();
            var toast = _this.toast.create({
                message: err,
                duration: 2000
            });
            toast.present();
            console.log(err);
        });
    };
    RegisterPage.prototype.loginFB = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            loading.dismiss();
            if (res.status === "connected") {
                _this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.isLoggedIn = false;
            }
            console.log('Logged into Facebook!', res);
        })
            .catch(function (e) {
            loading.dismiss();
            console.log('Error logging into Facebook', e);
            var toast = _this.toast.create({
                message: e,
                duration: 2000
            });
            toast.present();
        });
    };
    RegisterPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.http.get('http://tanboora.bit68.com/login/?fb_token=' + userid + '&email=' + res.email + '&name=' + res.name + '&image=' + res.picture.data.url).map(function (res) { return res.json(); }).subscribe(function (data) {
                console.log(data);
                if (data.id) {
                    _this.storage.set('user_info', data).then(function (val) {
                        console.log('storage value', val);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
                    });
                }
                else {
                    var toast = _this.toast.create({
                        message: data._body,
                        duration: 2000
                    });
                    toast.present();
                }
            }, function (err) {
                console.log(err);
                var toast = _this.toast.create({
                    message: err,
                    duration: 2000
                });
                toast.present();
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>إنشاء حساب</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div padding text-center>\n    <img src="assets/imgs/tanboora.png">\n  </div>\n  <ion-list>\n    <ion-item>\n      <!-- <ion-label stacked color="primary">Name</ion-label> -->\n      <ion-input placeholder="الاسم" [(ngModel)]="name"></ion-input>\n    </ion-item>\n    <ion-item>\n\n      <ion-input placeholder="البريد الالكتروني" [(ngModel)]="email"></ion-input>\n    </ion-item>\n    <ion-item>\n\n      <ion-input placeholder="كلمة المرور" type="password" [(ngModel)]="password"></ion-input>\n    </ion-item>\n    <ion-item>\n\n      <ion-input placeholder="التليفون" type="numbers" [(ngModel)]="phone"></ion-input>\n    </ion-item>\n    <ion-item class="noBottomLine">\n      <!-- <ion-label stacked color="primary">Brands</ion-label> -->\n      <ion-select [(ngModel)]="brand" interface="action-sheet">\n        <ion-option selected>الماركة</ion-option>\n        <ion-option>BMW</ion-option>\n        <ion-option>AUDI</ion-option>\n        <ion-option>KIA</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item class="noBottomLine">\n      <!-- <ion-label stacked color="primary">Models</ion-label> -->\n      <ion-select [(ngModel)]="model" interface="action-sheet">\n        <ion-option selected>موديل</ion-option>\n        <ion-option>Model 1</ion-option>\n        <ion-option>Model 2</ion-option>\n        <ion-option>Model 3</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item class="noBottomLine">\n      <!-- <ion-label stacked color="primary">Parts</ion-label> -->\n      <ion-select [(ngModel)]="part" interface="action-sheet">\n        <ion-option selected>أجزاء</ion-option>\n        <ion-option>Part 1</ion-option>\n        <ion-option>Part 2</ion-option>\n        <ion-option>Part 3</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked color="primary">أنا أقبل الشروط والأحكام</ion-label>\n      <ion-checkbox [(ngModel)]="terms"></ion-checkbox>\n    </ion-item>\n    <br>\n\n    <button ion-button block class="radBtn" [disabled]="!name ||!email||!password||!terms || !phone" (click)="register(name,email,password,phone,brand,model,part)">تسجيل</button>\n    <p ion-text color="gray" text-center>أو</p>\n    <button block ion-button (click)="loginFB()" class="fbBtn">تسجيل الدخول باستخدام الفيسبوك</button>\n\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__["a" /* Facebook */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderdetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OrderdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrderdetailsPage = (function () {
    function OrderdetailsPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.order = this.navParams.get('order');
        this.shipping = 100;
        this.subTotal = this.order.total + this.shipping;
        this.proc = 'goldenrod';
        this.confirm = 'green';
        this.Shipping = '#1970b9';
        this.deliv = '#283044';
        this.cancelled = '#db1e31';
    }
    OrderdetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrderdetailsPage');
    };
    OrderdetailsPage.prototype.openCart = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
        modal.present();
    };
    OrderdetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-orderdetails',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/orderdetails/orderdetails.html"*/'<!--\n  Generated template for the OrderdetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>#{{order.id}}</ion-title>\n    <!-- <ion-buttons dir="ltr" end>\n      <button (click)="openCart()" ion-button icon-only color="royal">\n        <ion-icon name="cart"><ion-badge color="secondary">3</ion-badge></ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h5 ion-text color="primary">معلومات الطلب</h5>\n  <ion-row *ngFor="let prod of order.items">\n    <ion-col col-2>\n      <img src="{{prod.product.image}}">\n    </ion-col>\n    <ion-col col-10>\n      <h5 ion-text color="primary">{{prod.product.title}}</h5>\n      <p ion-text color="gray">{{prod.product.price}}\n        <span ion-text color="secondary">({{prod.quantity}})</span>\n      </p>\n    </ion-col>\n\n  </ion-row>\n  <div class="separator"></div>\n  <ion-row >\n    <ion-col col-12 ion-text color="gray">حاصل الجمع : {{order.total}} جنيه مصري</ion-col>\n    <ion-col col-12 ion-text color="gray">شحن: {{shipping}} جنيه مصري</ion-col>\n    <ion-col col-12>\n      <b ion-text color="dark">مجموع : {{subTotal}} جنيه مصري</b>\n    </ion-col>\n  </ion-row>\n  <div class="separator"></div>\n<div class="title">\n  <h5 ion-text color="primary">متابعة حالة الطلب</h5>\n</div>\n  <!-- processing stat -->\n  <ion-row class="stats" *ngIf="order.status===\'Processing\'">\n    <ion-col [style.background-color]="\'goldenrod\'">\n      <span>Processing</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Confirmed</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Shipping</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Delivered</span>\n    </ion-col>\n  </ion-row>\n\n  <!-- confirmed stat -->\n  <ion-row class="stats" *ngIf="order.status===\'Confirmed\'">\n    <ion-col [style.background-color]="\'goldenrod\'">\n      <span>Processing</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'green\'">\n      <span >Confirmed</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Shipping</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Delivered</span>\n    </ion-col>\n  </ion-row>\n\n  <!-- shipping stat-->\n  <ion-row class="stats" *ngIf="order.status===\'Shipping\'">\n    <ion-col [style.background-color]="\'goldenrod\'">\n      <span>Processing</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'green\'">\n      <span>Confirmed</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'#1970b9\'">\n      <span>Shipping</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Delivered</span>\n    </ion-col>\n  </ion-row>\n\n  <!--delivered stat -->\n  <ion-row class="stats" *ngIf="order.status===\'Delivered\'">\n    <ion-col [style.background-color]="\'goldenrod\'">\n      <span>Processing</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'green\'">\n      <span ion-text color="lightGray">Confirmed</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'#1970b9\'">\n      <span ion-text color="lightGray">Shipping</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'#283044\'" >\n      <span ion-text color="lightGray">Delivered</span>\n    </ion-col>\n  </ion-row>\n\n  <!-- cancelled stat -->\n  <ion-row class="stats" *ngIf="order.status===\'Cancelled\'">\n    <ion-col [style.background-color]="\'goldenrod\'">\n      <span>Processing</span>\n    </ion-col>\n    <ion-col [style.background-color]="\'#db1e31\'">\n      <span ion-text color="lightGray">Cancelled</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Shipping</span>\n    </ion-col>\n    <ion-col>\n      <span ion-text color="lightGray">Delivered</span>\n    </ion-col>\n\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/orderdetails/orderdetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]])
    ], OrderdetailsPage);
    return OrderdetailsPage;
}());

//# sourceMappingURL=orderdetails.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AuthPage = (function () {
    function AuthPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AuthPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AuthPage');
    };
    AuthPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    AuthPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    AuthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-auth',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/auth/auth.html"*/'<ion-content>\n  <br><br>\n  <div padding text-center>\n    <img src="assets/imgs/tanboora.png">\n    <!-- <h3 ion-text color="light">TANBOORA</h3> -->\n  </div>\n\n</ion-content>\n<ion-footer>\n  <ion-row>\n    <ion-col>\n      <button block ion-button outline color="light" (click)="login()">الدخول</button>\n    </ion-col>\n    <ion-col>\n      <button block ion-button outline color="light" (click)="register()">تسجيل الدخول</button>\n    </ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/auth/auth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], AuthPage);
    return AuthPage;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlusaddressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PlusaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PlusaddressPage = (function () {
    function PlusaddressPage(navCtrl, loadingCtrl, storage, http, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
        this.events = events;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.storage.get('user_info').then(function (val) {
            console.log(val);
            if (val) {
                _this.myUser = val;
                loading.dismiss();
            }
        });
    }
    PlusaddressPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PlusaddressPage');
    };
    PlusaddressPage.prototype.add = function (area, add) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: ''
        });
        loading.present();
        this.http.get('http://tanboora.bit68.com/add_address/?user_id=' + this.myUser.id + '&area=' + area + '&details=' + add).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            loading.dismiss();
            _this.navCtrl.pop();
        }, function (err) {
            console.log(err);
            loading.dismiss();
        });
    };
    PlusaddressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-plusaddress',template:/*ion-inline-start:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/plusaddress/plusaddress.html"*/'\n<ion-header>\n\n  <ion-navbar color="caramel">\n    <ion-title>اضف عنوان</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n  <ion-item>\n    <ion-label ion-text color="light" stacked>المنطقة</ion-label>\n    <ion-input [(ngModel)]="area"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label ion-text color="light" stacked>تفاصيل العنوان</ion-label>\n    <ion-textarea [(ngModel)]="details" rows="4"></ion-textarea>\n  </ion-item>\n</ion-list>\n<div padding>\n  <button ion-button block color="light" (click)="add(area,details)">اضف</button>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/mohamedsaber/Desktop/Tanboora/src/pages/plusaddress/plusaddress.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], PlusaddressPage);
    return PlusaddressPage;
}());

//# sourceMappingURL=plusaddress.js.map

/***/ })

},[233]);
//# sourceMappingURL=main.js.map